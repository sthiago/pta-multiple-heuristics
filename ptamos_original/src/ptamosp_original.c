/*****************
80 0.005 0.005 .005 problemi/p4050n0.txt agc 0.0 20.0 100. 30 20 5 20
390 0.02 .005 .0020 problemi/p4050n0.txt agc 0.0 50.0 90. 300 20 5 20
90 0.002 0.0002 .0050 problemi/p2040n6.txt agc 0.0 10.0 90. 50 20 5 20
90 0.002 0.0002 .010 problemi/p2040n6.txt agc 0.0 10.0 90. 50 20 5 20

VLSI            TO MOSP
ACMO            DEZ/2002


NOVO ALGORITMO
HEURISTICA DO FAGIOLLI
VERSÃO FROM LINUX
COMPLETA PARA MOSP E VLSI
RECOMBINA-ESTRUTURAS E ESQUEMAS

2003
====

versao com poda que atualiza com proximo vizinho
podas sucessivas pra diminuir a populacao

Treinamento Populacional (no constructive) (APENAS)
**********/
#define MOSP "PTAMOS"
#define FAG
#define MOSPIN
#define NOLOG
#define NOREPRO
#define CONSO
#define ANSI
#define PERMUTA
#define XLS
#define NOMTX
#define DUMP
#define NOEXTRA

#ifdef NANSI
#pragma hdrstop
#include <condefs.h>
#pragma argsused
#endif

/****************************************/
/************** header files ************/
/****************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#ifdef NANSI
#include <memory.h>
#include <malloc.h>
#endif

/* Parametros de desempenho fixos*/

#define INFINITO 65535000
/* Simbolos e constantes*/
#define BASE 3
#define GUIA 2
#define FULL 1
#define NDEF 0
#define TRUE 1
#define FALSE 0
/* Herdados */
#define RAND 1
#define ABRE 1
#define FECHA 0
#define TAXAPD .8
#define SOLUCAO 14
#define AMOSTRA  5



/****************************************/
/*************** tipos ******************/
/****************************************/

typedef unsigned char  Uchar;
typedef unsigned int   Uint;
typedef unsigned long  Ulong;
typedef Uchar Byte;

struct MATRIZ {
	char  **string;
	Byte numgate, numnet;
}Matriz;

typedef struct INDIVIDUO {
	Byte * permuta;
	Byte * vizinho;
	float f, g;
	float delta, trilhas, trilhar, rank;
	Uchar tipo;
	Uint geracao;  /* quando foi criado */
	Uint sel, mut, col;
	float gp1, gp2, alf;
} Individuo;

typedef struct ELEMENTO {
	Individuo        *indi;
	struct ELEMENTO  *ante;
	struct ELEMENTO  *prox;
} Elemento;

typedef struct POPULACAO{
	Elemento *inicio, *fim;
	Uint tamanho;
	Uint naval;
}Populacao;

float PERC_ESQUEMAS,PERC_MELHORES, PERC_MUTACAO, FATOR;
int POPINI, PASSOS, MINPOP, NUMCRUZA, LARGO;
double d;


void Erro(int x);
Byte GeraIndividuoEsquema(Individuo * I);
void GeraIndividuoCompleto(Individuo * I);
Individuo *GeraIndividuoVazio (void);
Individuo * GeraIndividuoIgual ( Individuo * I);
void CalculaFDelta(Individuo *I);
void CalculaFOLD(Individuo * I);
void CalculaG(Individuo *I);
Individuo * GeraOpt(Individuo * I);
void IniciaPop ( Populacao * P);
void InsereNaPopulacao(Populacao * P, Individuo * I, double chave);
void Seleciona(Populacao P, Individuo **Base, Uchar tipo);
Individuo * PodaPopulacao ( Populacao *P, double alfa);
Individuo * RecombinaBG(Individuo *Base, Individuo *Guia);
Individuo * RecombinaPM(Individuo *base, Individuo *guia);
Individuo * RecombinaPE(Individuo *base, Individuo *guia);
Individuo * RecombinaES(Individuo *Base, Individuo *Guia);
Individuo * RecombinaBO(Individuo *base, Individuo *guia);
//void Esquematiza(Individuo *base);
//int LocalOpt(Individuo * I);
void Opt(Individuo * b, int p, int q);
Byte NaoPertence(Byte x, Byte permuta[], Uint tam);
int Mutacao(Individuo *Novo, float perc);
int MutacaoTotal(Individuo *Novo);
//int Mutacao2Opt(Individuo * I, float perc);
//int MutacaoACMO(Individuo * I, float perc);
int MostraInd ( FILE * s, Individuo *I);
int MostraPop (FILE * s, Populacao P);
//int MostraDois (FILE * s, Populacao P, int ger, float alf, float gmax, Individuo * m, Uint a, Uint b);
Individuo *CompletaSP(Individuo *E, Populacao P);
Individuo *CompletaHW ( Individuo * E);

FILE *ars;
Individuo *Melhor;
Populacao P;
double Gmax;

int main(int argc, char *argv[])
{

	Individuo *I, *Base, *Guia, *Novo, *NovoC;


	float di;
	double alfa;
	double inc;
	int iepson, fim;

	Byte i, j, k;
	char ch;

	FILE *arq;


	/* portabilizacao do codigo */
	int NUMNETS;
	int NUMGATES;


	Uint MAXLOOP;              /* linha de comando */
	float VARALF1;
	float VARALF2;
	char NARQ[51];
	char st[51];

	unsigned long t0,t1;
	Uint Geracao, Abortos, Nascidos, Cruzamentos;

	/**********************************/
	 /* inicializacao de parametros ****/
	 /**********************************/

	//srand(time(0));
	srand(0);

	if (argc < 12)
	{
			Erro (1);
			exit(1);
	}

	MAXLOOP = atoi(argv[1]);
	VARALF1 = atof(argv[2]);
	VARALF2 = atof(argv[3]);
	d       = atof(argv[4]);
	PERC_ESQUEMAS = atof (argv[7]);
	PERC_MELHORES = atof (argv[8]);
	PERC_MUTACAO  = atof (argv[9]);
	POPINI        = atof (argv[10]);
	PASSOS        = atof (argv[11]);
	if (argc > 12) NUMCRUZA = atof (argv[12]);
	else NUMCRUZA = 0;

	if (argc==14) LARGO = atof (argv[13]);
	else FATOR = 20;


	strcpy(NARQ,argv[5]);

	 /***************************************/
	 /* abre arquivo de matriz 01 e de saida*/
	 /***************************************/

	if (!(arq = fopen(NARQ, "r")))
	{
			Erro (2);
		return 2;
	}

	strcpy(st,argv[5]);

	if ( argc >= 7 )
		strcpy(&st[strlen(argv[5])-3],argv[6]);
	else
		st[strlen(argv[5])-4]=0;
	
	if (!(ars=fopen(st,"w"))){
		Erro(20);
		exit(20);
	}

	#ifdef VLSIIN
	fscanf(arq, "%d %d", &NUMGATES, &NUMNETS);
	#endif

	#ifdef MOSPIN
	fscanf(arq, "%d\n%d\n",  &NUMGATES, &NUMNETS);
	#endif

	MINPOP = 10;
	if (NUMCRUZA==0) NUMCRUZA = POPINI/10;

	/*********************/
	/* mostra parametros */
	/*********************/

	#ifdef CONSO
	fprintf(stdout,"\n>>> ALGORITMO PARA TREINAMENTO POPULACIONAL ..................(AcMo)\n");
	fprintf(stdout,"    %s em %s\n\n", MOSP, argv[0]);
	fprintf(stdout,"Gates..........: %d\n", NUMGATES);
	fprintf(stdout,"Nets...........: %d\n", NUMNETS);
	fprintf(stdout,"Max Loop.......: %d\n", MAXLOOP);
	fprintf(stdout,"Var Alfa 1.....: %.4f\n", VARALF1);
	fprintf(stdout,"Var Alfa 2.....: %.4f\n", VARALF2);
	fprintf(stdout,"Desvio.........: %.4f\n", d);
	fprintf(stdout,"Matriz ........: %s\n", NARQ);
	fprintf(stdout,"Indefinição #..: %.4f (%%)\n", PERC_ESQUEMAS);
	fprintf(stdout,"Melhores.......: %.4f (%%)\n", PERC_MELHORES);
	fprintf(stdout,"Mutação........: %.4f (%%)\n", PERC_MUTACAO);
	fprintf(stdout,"Pop. Inicial...: %d \n", POPINI);
	fprintf(stdout,"Pop. Minima....: %d \n", MINPOP);
	fprintf(stdout,"Passos Mutacao.: %d \n", PASSOS);
	fprintf(stdout,"Num.Cruzamentos: %d \n", NUMCRUZA);
	fprintf(stdout,"Largura........: %d \n", LARGO);
	fprintf(stdout,"Arquivo Saída..: %s \n", st);
	#endif

	#ifdef NOXLS
	fprintf(ars,"\n>>> ALGORITMO PARA TREINAMENTO POPULACIONAL .............(AcMo)\n");
	fprintf(ars,"    %s em %s\n\n", MOSP, argv[0]);
	fprintf(ars,"Gates..........: %d\n", NUMGATES);
	fprintf(ars,"Nets...........: %d\n", NUMNETS);
	fprintf(ars,"Max Loop.......: %d\n", MAXLOOP);
	fprintf(ars,"Var Alfa 1.....: %.4f\n", VARALF1);
	fprintf(ars,"Var Alfa 2.....: %.4f\n", VARALF2);
	fprintf(ars,"Desvio.........: %.4f\n", d);
	fprintf(ars,"Matriz ........: %s\n", NARQ);
	fprintf(ars,"Indefinição #..: %.4f (%%)\n", PERC_ESQUEMAS);
	fprintf(ars,"Melhores.......: %.4f (%%)\n", PERC_MELHORES);
	fprintf(ars,"Mutação........: %.4f (%%)\n", PERC_MUTACAO);
	fprintf(ars,"Pop. Inicial...: %d \n", POPINI);
	fprintf(ars,"Pop. Minima....: %d \n", MINPOP);
	fprintf(ars,"Passos Mutacao.: %d \n", PASSOS);
	fprintf(ars,"Num.Cruzamentos: %d \n", NUMCRUZA);
	fprintf(ars,"Largura........: %d \n", LARGO);
	fprintf(ars,"Arquivo Saída..: %s \n", st);
	#endif

	#ifdef CONSO
	puts("<ENTER>/<ESC>?");
		if (27 == getchar()) {
			Erro(3);
			exit(3);
		}
	#endif

	if (PERC_ESQUEMAS > 0.0F) { /// PTA ONLY
			Erro(999);
			exit(999);
	}

	/****************************/
	/* Aloca e  monta matriz 01 */
	/****************************/

	#ifdef CONSO
		puts("Alocando memória inicial ...");
	#endif

	#ifdef MOSPIN
	Matriz.string = (char **) malloc(NUMNETS*sizeof(char *));
	if (!Matriz.string) {
		Erro(4);
		exit(4);
	}
	for (i = 0; i < NUMNETS; i++) {
		Matriz.string[i] = (char *) malloc(NUMGATES*sizeof(char));
		if (!Matriz.string[i]) {
			Erro(5);
			exit(5);
		}
	}

	for (i = 0; i < NUMGATES && !feof(arq); i++)
	{
		for (j = 0; j < NUMNETS && !feof(arq);)
		{
			fscanf(arq, "%c\n", &ch);
			if (ch == '0') {
				Matriz.string[j][i] = '0';
				j++;
			} else {
				Matriz.string[j][i] = '1';
				j++;
			}
		}
	}
	fclose(arq);
	if (j != NUMNETS || i != NUMGATES){
		Erro(6);
		exit(6);
	}
	#endif

	#ifdef VLSIIN
	Matriz.string = (char **) malloc(NUMNETS*sizeof(char *));
	if (!Matriz.string) {
		Erro (4);
		exit(4);
	}
	for (i=0; i< NUMNETS ; i++) {
		Matriz.string[i] = (char *) malloc(NUMGATES*sizeof(char));
		if (!Matriz.string[i]) {
			Erro(5);
			exit(5);
		}
	}

	for(i=0; i<NUMNETS && !feof(arq); i++)
	{
		for(j=0; j<NUMGATES && !feof(arq);)
		{
			fread(&ch, sizeof(char), 1, arq);
			if (ch=='1' || ch =='0') {
				Matriz.string[i][j] = ch;
				j++;
			}
		}
	}
	fclose(arq);
	if (i!=NUMNETS || j !=NUMGATES){
		Erro (6);
		exit(6);
	}
	#endif
	
	/* Pode haver truncamento de números inteiros*/
	Matriz.numnet= NUMNETS;
	Matriz.numgate = NUMGATES;
	IniciaPop(&P);
	/* Calcula GMAX ; aloca um individuo, gera, calcula g*/
	Melhor=GeraIndividuoVazio();
	GeraIndividuoCompleto(Melhor);

	puts("DEBUG MELHOR INDIVIDUO:");
	MostraInd(stdout, Melhor);

	CalculaG(Melhor);    /* G, trilhas e tipo (Esq ou Com) */
	printf("g = %f", Melhor->g);

	CalculaFDelta(Melhor);
	printf("f = %f, delta = %f", Melhor->f, Melhor->delta);

	Gmax=2*Melhor->g;
	Melhor->geracao=0; /*geracao chute*/

	#ifdef CONSO
		puts("Gerando população inicial ...");
	#endif

	/*Gera Populacao Inicial */
	while (P.tamanho < POPINI) {
		I = GeraIndividuoVazio();
		k = GeraIndividuoEsquema(I);
		CalculaG(I);
		if (I->g >= Gmax) {      /* atualiza Gmax com o novo valor */
			Gmax = 2 * (I->g + 1);
		}
		CalculaFDelta(I);
		if (I->f < Melhor->f) {  // aqui g = f
			free(Melhor->permuta);
			free(Melhor->vizinho);
			free(Melhor);
			Melhor = GeraIndividuoIgual(I);
		}

		I->geracao = 1; /*geracao inicial */
		InsereNaPopulacao(&P, I, I->delta);
	}

	#ifdef CONSO
		puts("Evolucao Natural ...\n\n\n\n\n");
	#endif

	Geracao = 0;

	t0 = time(0);
	fim = AMOSTRA;
	alfa = P.fim->indi->delta;
	while ( (P.tamanho > MINPOP) && (Geracao <= MAXLOOP || fim > 0) && (Melhor->trilhar != SOLUCAO))
	{

	#ifdef LOG
	if ((Geracao%AMOSTRA)==0) fprintf(ars, " %d; ", (int) Melhor->g);
	#endif
	
	Geracao++;
	fim--;
	Cruzamentos = 0;
	while (Cruzamentos++ < NUMCRUZA)
	{
		Seleciona(P, &Base, BASE);
		Seleciona(P, &Guia, GUIA);
		if ((rand() % 100) < PERC_MUTACAO) {
			k = PASSOS;
			NovoC = GeraIndividuoIgual(Base);
			while (k-- && Mutacao(NovoC, PERC_MUTACAO));
			NovoC->geracao = Geracao;
			NovoC->alf = alfa;
			CalculaFDelta(NovoC);

			if (NovoC->f < Melhor->f) {  // aqui g = f
				fim++;
				free(Melhor->permuta);
				free(Melhor->vizinho);
				free(Melhor);
				Melhor = GeraIndividuoIgual(NovoC);
				k = PASSOS;
				printf("\n+(B) Ger=%d, Pop=%d, a= %.4f, p=%d, f=%d, ** %d(%d) **, %.4f", Geracao, P.tamanho, alfa, k, (int) Melhor->f, (int) (Melhor->f - (Matriz.numgate * Matriz.numnet *Melhor->trilhar)), (int) Melhor->trilhar, Melhor->delta);
			}

			if (alfa < NovoC->delta)
				InsereNaPopulacao(&P, NovoC, NovoC->delta);
			else    {
				free(NovoC->permuta);
				free(NovoC->vizinho);
				free(NovoC);
			}
		}

		if (Base == Guia)
			continue;

		Novo = RecombinaBO(Base, Guia);
		CalculaG(Novo);
		Novo->geracao = Geracao;
		Novo->alf = alfa;
		CalculaFDelta(Novo);

		if (Novo->f < Melhor->f) {  // aqui g = f
			fim++;
			free(Melhor->permuta);
			free(Melhor->vizinho);
			free(Melhor);
			Melhor = GeraIndividuoIgual(Novo);
			k = PASSOS;
			printf("\n+(N) Ger=%d, Pop=%d, a= %.4f, p=%d, f=%d, ** %d(%d) **, %.4f", Geracao, P.tamanho, alfa, k, (int) Melhor->f, (int) (Melhor->f - (Matriz.numgate * Matriz.numnet *Melhor->trilhar)), (int) Melhor->trilhar, Melhor->delta);
		}

		if (alfa < Novo->delta)
			InsereNaPopulacao(&P, Novo, Novo->delta);
		else    {
			free(Novo->permuta);
			free(Novo->vizinho);
			free(Novo);
		}

	}

//        alfa = P.fim->indi->delta + VARALF1*P.fim->indi->delta ;
//        alfa    = alfa*(P.fim->indi->delta - P.inicio->indi->delta);
//        alfa    = -alfa +P.fim->indi->delta;

//        k=PASSOS;

	alfa = P.fim->indi->delta; // + VARALF1*(P.inicio->indi->delta - P.fim->indi->delta)/Geracao ;

	while (alfa >= P.fim->indi->delta && P.tamanho){
		Novo = PodaPopulacao(&P, alfa);
		if (Novo == NULL) break;
		memcpy(Novo->permuta, Novo->vizinho, Matriz.numgate);
		Novo->g = Novo->f;
		Novo->trilhas = Novo->trilhar;
		CalculaFDelta(Novo); // reinsere
		if (Novo->f < Melhor->f) {  // aqui g = f
			fim++;
			free(Melhor->permuta);
			free(Melhor->vizinho);
			free(Melhor);
			Melhor = GeraIndividuoIgual(Novo);
			k = PASSOS;
			printf("\n+(P) Ger=%d, Pop=%d, a= %.4f, p=%d, f=%d, ** %d(%d) **, %.4f", Geracao, P.tamanho, alfa, k, (int) Melhor->f, (int) (Melhor->f - (Matriz.numgate * Matriz.numnet *Melhor->trilhar)), (int) Melhor->trilhar, Melhor->delta);
		}
		if (alfa < Novo->delta)
			InsereNaPopulacao(&P, Novo, Novo->delta);
		else    {
			free(Novo->permuta);
			free(Novo->vizinho);
			free(Novo);
		}
	}
#ifdef CONSO

	if (!(Geracao % AMOSTRA) ) printf("\n#### Ger=%d, Pop=%d, a= %.4f, %d-%d, ** %d(%d) **",Geracao,P.tamanho, alfa, (int) Melhor->f, (int) Melhor->g, (int) (Melhor->f - (Matriz.numgate * Matriz.numnet *Melhor->trilhar)), (int)Melhor->trilhar);

#endif
}

/*-------------------- R E S U L T A D O S -----------*/
	t1 = time(0);
	printf("\n%s) Saida: %s; Aval: %d\n\n\n\n", MOSP, st, P.naval);

/* ------------------- Testa condicao de termino -----*/
	if (P.tamanho <=MINPOP)         fim=3;
		if (Geracao  > MAXLOOP)         fim=2;
		if (Melhor->trilhar == SOLUCAO) fim=1;

#ifdef CONSO
		puts("\n\n\nFinalizando por ...");
		if (P.tamanho <= MINPOP)         printf("Populacao.. : %d!", P.tamanho);
		if (Geracao > MAXLOOP)         printf("Geracao.. :   %d!", Geracao);
		if (Melhor->trilhar == SOLUCAO) printf("Prob.Solucao :%d!", Melhor->trilhar);
		if (fim <= 0)                     printf("Cond.Termino :%d!", fim);
#endif

#ifdef EXTRA
	 MutacaoTotal(Melhor);
#endif

#ifdef XLS

		 fprintf(ars, "\n%s; %s;",MOSP, st);
		 fprintf(ars, " %d; %u; %d; %d; %u; %u", Geracao, t1-t0, (int) (Melhor->f - (Matriz.numgate * Matriz.numnet *Melhor->trilhar)), (int) Melhor->trilhar, Melhor->geracao, P.naval);
		 // fprintf(ars, " %.4f; %d;",alfa, P.tamanho);
#else
		 fprintf(ars, "\n%s%d: %s",MOSP, Geracao, st);
		 fprintf(ars,"\n\n\n %s ", MOSP);
		 fprintf(ars,"\n          Alexandre César Oliveira - 2000");
		 fprintf(ars,"\n          Gate minimization");
		 fprintf(ars,"\n\n         Entrada: %d, %.4f, %.4f, %.4f, %s", MAXLOOP, VARALF1, VARALF2, d, argv[5]);
		 fprintf(ars,"\n          Matriz...:");
		 for (j=0;j<Matriz.numgate;j++){
				fprintf(ars,"\n\r");
				for (i=0; i < Matriz.numnet ; i++)
						fprintf (ars, " %c ", Matriz.string[i][j]);
		 }
		 fprintf(ars,"\n          Tempo: %ul a %ul = %ul", t0, t1, t1-t0);
		 fprintf(ars,"\n          Ger: %d(%.4f), Pop: %d, Gmax: %.4f, Aval: %u", Geracao, alfa, P.tamanho, Gmax, P.naval);
		 fprintf(ars,"\n          Melhores...\n");
		 MostraInd(ars, Melhor);
#endif

#ifdef CONSO
		 printf("\n ### FINAL Ger = %d, Pop = %d, ** %d(%d) **",Geracao,P.tamanho,   (int) (Melhor->f - (Matriz.numgate * Matriz.numnet *Melhor->trilhar)), (int)Melhor->trilhar);
		 getchar();
#endif

#ifdef DUMP
		 fprintf(ars,"\n          Populacao...\n");
		 MostraPop(ars, P);
#endif
		 if (P.tamanho > 1)
				PodaPopulacao(&P, P.inicio->indi->delta+1.0F);
} // Fim main

int MostraInd ( FILE * s, Individuo *I)
{     int i, j;

#ifdef MTX
		for (j=0; j < Matriz.numgate;j++) {
				fprintf(s,"\n\r");
				for (i=0; i < Matriz.numnet ; i++)
						if ( I->vizinho[j] != NDEF )
								fprintf(s," %c ", Matriz.string[i][I->vizinho[j]-1]);
						else    fprintf(s, " ? ");
		}
		fprintf(s,"\n\n\r");
#endif
#ifdef PERMUTA
		fprintf(s,"\n\r");
		for (j=0; j < Matriz.numgate; j++)
				if (I->permuta[j] != NDEF )
						fprintf(s, " %d ",I->permuta[j]);
				else
						fprintf(s, " # ");
		fprintf(s,"\n\r");
		for (j=0; j < Matriz.numgate; j++)
				if (I->vizinho[j] != NDEF )
						fprintf(s, " %d ",I->vizinho[j]);
				else
						fprintf(s, " # ");

#endif

		fprintf(s, "\n%c) G=%d (%d), F=%d (%d), ger/alf=%d/%.2f, %.6f, col/mut/sel=(%d,%d,%d), rank=%.4f\n",
				 (I->tipo==FULL?'C':'E'),(int)I->g ,(int)I->trilhas,(int)I->f,(int)I->trilhar,I->geracao,I->alf,I->delta, I->col, I->mut, I->sel, I->rank);
}

int MostraPop(FILE *s, Populacao P)
{
	Elemento * aux = P.inicio;
	int i = 1;

	for (aux = P.inicio; aux != (Elemento *) NULL;
	     i++, aux = aux->prox){
		MostraInd(s, aux->indi);
	}
}

/*      NaoPertence retorna o item x se ele nao pertence
		ou retorna NDEF caso x ja pertenca a permutacao
		Retornar x eh util na rotina de Cruzamento*/
Byte NaoPertence(Byte x, Byte permuta[], Uint tam)
{
	Uint i;
	for (i = 0; i < tam; i++)
		if (permuta[i] == x) return NDEF;
	return x;
}

Individuo *PodaPopulacao(Populacao *P, double alfa)
{
	Elemento *aux;
	Individuo *I = (Individuo *) NULL;
	if (P->fim == (Elemento *) NULL)
		return I;
	else {
		aux = P->fim;
		do {
			if (aux->indi->delta <= alfa) {
				aux = aux->ante;
				P->fim = aux;
				if (aux != (Elemento*) NULL) {
					if (aux->prox->indi->f == aux->prox->indi->g){
						free(aux->prox->indi->vizinho);
						free(aux->prox->indi->permuta);
						free(aux->prox->indi);
						free(aux->prox);
						aux->prox = (Elemento*) NULL;
						P->tamanho--;
					} else {
						I = aux->prox->indi; //salva indi
						free(aux->prox);
						aux->prox = (Elemento*) NULL;
						P->tamanho--;              // delimita e exclui
						break;
					}
				}
			}
		} while (aux != (Elemento*) NULL && aux->indi->delta <= alfa);

		if (aux == (Elemento*) NULL) {
			free(P->inicio);
			P->inicio = P->fim = (Elemento*) NULL;
		}
	}

	return(I);
}
/*void PodaPopulacaOLD ( Populacao *P, double alfa)
{
		Elemento *aux;
		int x=0;
		if (P->fim == (Elemento *)NULL)
				return;
		else {
				aux=P->fim;
				do {
						if (aux->individuo.delta >= alfa ){
								aux=aux->ante;
								if (aux != (Elemento*) NULL) {
										free(aux->prox);
										aux->prox=(Elemento*) NULL;
										P->tamanho --;
								}
						}
				}while ( aux != (Elemento*) NULL && aux->individuo.delta >= alfa );
				if (aux == (Elemento*) NULL){
						free(P->inicio);
						P->inicio=P->fim=(Elemento*) NULL;
				}
				else
						P->fim=aux;
		}

} */

void Seleciona(Populacao P, Individuo **Base, Uchar tipo)
{
	float percent = 100.0; /*considera todos */
	Byte escolha;
	Elemento *p;


	if (tipo == BASE) percent = PERC_MELHORES * P.tamanho / 100.0;
	if (tipo == GUIA) percent = P.tamanho;
	escolha = (Byte) rand() % (int) (percent + 1);
	for (p = P.inicio; p != P.fim && escolha; p = p->prox) escolha--;
	p->indi->sel++;
	*Base = p->indi;
}


void GeraIndividuoCompleto(Individuo *I)
{
	Byte i,j,pos;
	
	for (i = 0; i < Matriz.numgate; i++)
		I->permuta[i] = NDEF;

	for (i = 0; i < Matriz.numgate ; i++) {
		pos = (Byte) rand() % Matriz.numgate;

		while (I->permuta[pos] != NDEF)
			pos = (++pos) % Matriz.numgate;

		I->permuta[pos] = i + 1;
	}

	I->tipo = FULL;
}

Byte GeraIndividuoEsquema(Individuo *I)
{
	Byte i, pos, val, inc, esquemas, *usado;

	usado = (Byte *) malloc(Matriz.numgate * sizeof(Byte));
	if (!usado) { Erro(70); exit(70); }

	for (i = 0; i < Matriz.numgate; i++)
		usado[i] = I->permuta[i] = NDEF;

	esquemas = Matriz.numgate * (1 - PERC_ESQUEMAS/100.00);
	
	for (i=0; i < esquemas ; i++) {
		val = (Byte) rand() % Matriz.numgate;

		while (usado[val] != NDEF)
			val = val < Matriz.numgate-1 ? val+1 : 0;

		usado[val]=TRUE;

		pos = (Byte) rand() % Matriz.numgate;

		while (I->permuta[pos] != NDEF)
			pos = pos < Matriz.numgate-1 ? pos+1 : 0;

		I->permuta[pos] = val+1;
	}

	if (esquemas < Matriz.numgate) I->tipo = !FULL;
	else I->tipo = FULL;

	I->sel = I->mut = I->col = 0;

	return(esquemas);
}

void CalculaG(Individuo *I)
{
	float acum;
	int i, j, max, custo, defs;
	Byte *esq, *dir;

	esq = (Byte *) malloc(sizeof(Byte) * Matriz.numnet);
	dir = (Byte *) malloc(sizeof(Byte) * Matriz.numnet);
	if (!esq || !dir) { Erro(12); exit(12); }

	for (j = 0; j < Matriz.numnet; j++) {
		esq[j] = dir[j] = 0;
		for (i = 0; i < Matriz.numgate; i++) {
			if (I->permuta[i] != NDEF) {
				if (Matriz.string[j][I->permuta[i] - 1] == '1') {
					if (!esq[j])    esq[j] = i + 1;
					else            dir[j] = i + 1;
				}
			}
		}
		if (!dir[j]) dir[j] = esq[j];
	}

	/* calculo das trilhas, custo e do numero de #´s */
	for (i = 0, max = 0, custo = 0, defs = 0; i < Matriz.numgate; i++){
		if (I->permuta[i] != NDEF) {
			defs++;
			for (j = 0, acum = 0; j < Matriz.numnet; j++)
				if (i >= esq[j] - 1 && i <= dir[j] - 1) acum += 1;
			max = (acum > max ? acum : max);
			custo += acum;
		}
	}

	I->f = I->g = Matriz.numgate * Matriz.numnet * max + custo;
	I->trilhas = max;
	I->tipo = (defs == Matriz.numgate ? FULL : !FULL);
	// I->rank=(float) defs; /* temporariamente ranking é ndefs*/

	free(esq);
	free(dir);

	P.naval++;
}

int Mutacao(Individuo *I, float percent)
{
	Individuo J, K;
	int i, j, sigmax = 0;
	Byte x, y, z;

	J.permuta = (Byte *) malloc(sizeof(Byte) * Matriz.numgate);
	K.permuta = (Byte *) malloc(sizeof(Byte) * Matriz.numgate);
	if (J.permuta == NULL || K.permuta == NULL) { Erro(335); exit(335); }

	y = (Matriz.numgate <= LARGO ? 0 : rand() % (Matriz.numgate - LARGO));
	z = y + (Matriz.numgate <= LARGO ? Matriz.numgate : LARGO);

	for (i = y, sigmax = 0; i < z; i++){
		for (j = i + 1; j < z; j++){
			memcpy(J.permuta, I->permuta, Matriz.numgate);
			Opt(&J, i, j);
			CalculaG(&J);
			if (I->g - J.g > sigmax) {
				memcpy(K.permuta, J.permuta, Matriz.numgate);
				K.g = J.g;
				K.trilhas = J.trilhas;
				K.tipo = J.tipo;
				K.rank = J.rank;
				sigmax = I->g - J.g;
			}
		}
	}

	if (sigmax > 0) {
		memcpy(I->permuta, K.permuta, Matriz.numgate);
		I->g = I->f = K.g;
		I->trilhas = K.trilhas;
		I->tipo = K.tipo;
		I->rank = K.rank; /* temporariamente ranking é ndefs*/
		I->mut++;
		free(J.permuta);
		free(K.permuta);
		return TRUE;
	}
	free(J.permuta);
	free(K.permuta);
	return FALSE;
}

int MutacaoTotal(Individuo * I)
{
	Individuo J, K;
	int i, j, sigmax = 0;
	if (I->tipo == FULL) {
		J.permuta = (Byte *) malloc(sizeof(Byte) * Matriz.numgate);
		K.permuta = (Byte *) malloc(sizeof(Byte) * Matriz.numgate);
		if (J.permuta == NULL || K.permuta == NULL) {
			Erro(336);
			exit(336);
		}

		for (i = 0, sigmax = 0; i <= Matriz.numgate; i++){
			for (j = i + 1; j < Matriz.numgate; j++){
				memcpy(J.permuta, I->permuta, Matriz.numgate);
				Opt(&J, i, j);
				CalculaG(&J);
				if (I->g - J.g > sigmax) {
					memcpy(K.permuta, J.permuta, Matriz.numgate);
					K.g = J.g;
					K.trilhas = J.trilhas;
					K.tipo = J.tipo;
					K.rank = J.rank; /* temporariamente ranking é ndefs*/
					sigmax = I->g - J.g;
				}
			}
		}
		if (sigmax > 0) {
			memcpy(I->permuta, K.permuta, Matriz.numgate);
			I->g = I->f = K.g;
			I->trilhas = K.trilhas;
			I->tipo = K.tipo;
			I->rank = K.rank; /* temporariamente ranking é ndefs*/
			I->mut++;
			free(J.permuta);
			free(K.permuta);
			return TRUE;
		}
	}
	free(J.permuta);
	free(K.permuta);
	return FALSE;
}

Individuo *GeraOpt(Individuo * I)
{
	Individuo *J;
	int i, j, sigmax = 0;

	/* Aloca novo individuo */
	J = GeraIndividuoVazio();
	for (i = 0; i < Matriz.numgate; i++){
		J->permuta[i] = I->permuta[i];
		J->g = J->f = I->g;  /*faz f e g como sendo a principio o mesmo valor*/
	}
	sigmax = 0;
	for (i = 0; i <= Matriz.numgate - 4; i++)
		for (j = 2; j <= Matriz.numgate - 2; j++){
			J->permuta[j - 1] = I->permuta[j - 1]; /* salva uma posicao */
			J->permuta[j] = I->permuta[i];
			J->permuta[j + 1] = I->permuta[i + 1];
			J->permuta[i] = I->permuta[j];
			J->permuta[i + 1] = I->permuta[j + 1];
			CalculaG(J);
			if (I->g - J->g > sigmax) {
				sigmax = I->g - J->g;
				J->f = J->g;
			}
		}
	J->g = I->f = J->f; /* no final o pega-se o melhor g do auxiliar J e coloca-se em I */
	return(J);
}
/* Insere ordenando pelo ranking do individuo */
void InsereNaPopulacao(Populacao *P, Individuo *I, double chave)
{
	Elemento * E, *aux;
	Byte iguais = FALSE;

	E = (Elemento *) malloc(sizeof(Elemento));
	if (!E) { Erro(9); exit(9); }

	E->indi = I;
	E->prox = (Elemento *) NULL;
	E->ante = (Elemento *) NULL;

	if (P->inicio == (Elemento *) NULL) {
		P->inicio = P->fim = E;
		P->tamanho++;
	} else {
		for (aux = P->inicio; aux != (Elemento *) NULL && chave < aux->indi->delta; aux = aux->prox)
			;

		for (; aux != (Elemento *) NULL && chave == aux->indi->delta; aux = aux->prox) {
			if (!memcmp(E->indi->permuta, aux->indi->permuta, Matriz.numgate)) {
				E->indi->col++;
				aux->indi->col++;
				iguais = TRUE;
				break;
			}
		}

		if (!iguais) {
			if (aux){      // nao chegou no fim
				E->prox = aux;
				E->ante = aux->ante;
				aux->ante = E;
				if (E->ante == (Elemento *) NULL)
					P->inicio = E;
				else
					E->ante->prox = E;
			} else {
				E->prox = (Elemento *) NULL;
				E->ante = P->fim;
				P->fim = E;
				E->ante->prox = E;
			}
			P->tamanho++;
		} else {
			free(E->indi->permuta);
			free(E->indi->vizinho);
			free(E);
		}
	}
}

void IniciaPop(Populacao * P)
{
	P->inicio = P->fim = (Elemento *) NULL;
	P->tamanho = 0;
	P->naval = 0;
}

void Erro (int x)
{
	switch (x){
	case 1:        printf("Faltam argumentos, sao 11...\n");
		printf("LOOP VARALF1 VARALF2 d Arq.txt saida %%ESQ %%BASE %%MUT POPINI K-MUT [XOVER LARGO]\n");
		break;
	case 2:
		printf("Erro na Abertura do Arquivo\n");
		break;
	case 3:
		printf("\nSolicitação do usuario\n");
		break;
	case 4:
		printf("\nAlocação de memória para a matriz #4\n");
		break;
	case 5:
		printf("\nAlocação de memória para a matriz #5\n");
		break;
	case 6:
		printf("\nMatriz Inválida\n");
		break;
	case 10:
		printf("\nFalta memória para elemento da lista\n");
		break;
	case 11:
		printf("\nFalta memória para indivíduo\n");
		break;
	case 12:
		printf("\nFalta Memória para G()\n");
		break;
	default:
		printf("\nErro desconhecido...(%d)", x);
	}
	getchar();
}

Individuo * RecombinaBG(Individuo *Base, Individuo *Guia)
{
		Individuo *p;
		Byte i;
		Uchar tipo=FULL;
/* Aloca novo individuo */
		p=GeraIndividuoVazio();
		for (i=0; i < Matriz.numgate; i++){
				if (Base->permuta[i] == NDEF &&
					Guia->permuta[i] == NDEF )
					p->permuta[i] =  NDEF;
				if (Base->permuta[i] == NDEF &&
					Guia->permuta[i] != NDEF )
					p->permuta[i] =  NaoPertence(Guia->permuta[i], p->permuta, i);/* Guia ou # */
				if (Base->permuta[i] != NDEF &&
					Guia->permuta[i] == NDEF )
					p->permuta[i] =  NaoPertence(Base->permuta[i], p->permuta, i);/* Base ou # */
				if (Base->permuta[i] != NDEF &&
					Guia->permuta[i] != NDEF ){
					if ((p->permuta[i] =  NaoPertence(Base->permuta[i], p->permuta, i))==NDEF)/* Base ou # */
						 p->permuta[i] =  NaoPertence(Guia->permuta[i], p->permuta, i);
				} /* Se Base[i] ja incluido tenta Guia[i] se tambem, coloca #*/
				if (p->permuta[i] == NDEF) tipo=!FULL;
	  }
p->tipo=tipo;
p->gp1=Base->g;
p->gp2=Guia->g;
return p;
}
/* recobina estruturas e esquemas pelo BASE GUIA*/
Individuo * RecombinaES(Individuo *Base, Individuo *Guia)
{
		Individuo *p;
		Byte i, x, y;
		Uchar tipo=FULL;
		int faixa;
/* Aloca novo individuo */
		p=GeraIndividuoVazio();
		x = rand()%2;
		faixa = rand()%Matriz.numgate + 1;

		for (i=0; i < Matriz.numgate; i++){
				if (Base->permuta[i] == NDEF &&
					Guia->permuta[i] == NDEF )
					p->permuta[i] =  NDEF;
				if (Base->permuta[i] == NDEF &&
					Guia->permuta[i] != NDEF )
					p->permuta[i] =  NaoPertence(Guia->permuta[i], p->permuta, i);/* Guia ou # */
				if (Base->permuta[i] != NDEF &&
					Guia->permuta[i] == NDEF )
					p->permuta[i] =  NaoPertence(Base->permuta[i], p->permuta, i);/* Base ou # */
				if (Base->permuta[i] != NDEF &&
					Guia->permuta[i] != NDEF ){
					if (x) {
						if ((p->permuta[i] =  NaoPertence(Base->permuta[i], p->permuta, i))==NDEF)/* Base ou # */
						p->permuta[i] =  NaoPertence(Guia->permuta[i], p->permuta, i);
					}
					else {
						if ((p->permuta[i] =  NaoPertence(Guia->permuta[i], p->permuta, i))==NDEF)/* Guia ou # */
						p->permuta[i] =  NaoPertence(Base->permuta[i], p->permuta, i);
					}
				}
				if (p->permuta[i] == NDEF) tipo=!FULL;
				if ((i+1) % faixa == 0)
						x = !x;
	  }
p->tipo=tipo;
p->gp1=Base->g;
p->gp2=Guia->g;
return p;
}

int Custo(Byte a, Byte b)
{
	Byte i, quebras = 0;
	if ( a == 0 || b == 0) return 0;

	for (i = 0; i < Matriz.numnet; i++)
		if (Matriz.string[i][a-1] != Matriz.string[i][b-1])
			quebras++;
	return quebras;
}

Individuo *RecombinaBO(Individuo *base, Individuo *guia)
{
	int i, j, k, cont;
	Individuo *p;
	Byte *q;

	if (base->tipo != FULL || guia->tipo !=FULL)
			return (Individuo *) NULL;

	/* Aloca novo individuo */
	p = GeraIndividuoVazio();
	q = (Byte*) malloc(sizeof(Byte) * Matriz.numgate);
	cont = Matriz.numgate;
	k = Matriz.numgate/2;


	for (i = 0; i < Matriz.numgate; i++)
		p->permuta[i] = q[i] = 0;

	for (i = 0; i <= k; i++){
		if ((j = rand() % Matriz.numgate)) {
			p->permuta[j] = base->permuta[j];
			cont -= !q[p->permuta[j]-1];
			q[p->permuta[j]-1] = 1;
		}
	}

	for (i = 0, j = 0; i < Matriz.numgate && cont; i++) {
		if (!p->permuta[i]) {
			while (q[guia->permuta[j++]-1])
				;
			p->permuta[i] = guia->permuta[j-1];
			cont--;
		}
	}

	p->gp1 = base->g;
	p->gp2 = guia->g;
	p->tipo = FULL;

	free(q);
	return(p);
}

Individuo *RecombinaPM(Individuo *base, Individuo *guia)
{
int ponto,comprimento,i,j,k;
unsigned tmp;

Individuo *p, *q;

if (base->tipo != FULL || guia->tipo !=FULL)
		return (Individuo *) NULL;
/* Aloca novo individuo */
p=GeraIndividuoVazio();
q=GeraIndividuoVazio();

memcpy(p->permuta, base->permuta, Matriz.numgate);
memcpy(q->permuta, guia->permuta, Matriz.numgate);

/* Randomicamente um ponto de PAR 0 */

ponto = rand()%(Matriz.numgate);
comprimento = TAXAPD * Matriz.numgate;

for (i=0;i<comprimento; i++){
	for (j=0;j<Matriz.numgate; j++)
		if (p->permuta[j] == q->permuta[(ponto+i)%Matriz.numgate])
			break;
	for (k=0;k<Matriz.numgate; k++)
		if (q->permuta[k] == p->permuta[(ponto+i)%Matriz.numgate])
			break;
	tmp=q->permuta[(ponto+i)%Matriz.numgate];
	q->permuta[(ponto+i)%Matriz.numgate] = p->permuta[(ponto+i)%Matriz.numgate];
	p->permuta[j]  = p->permuta[(ponto+i)%Matriz.numgate];
	p->permuta[(ponto+i)%Matriz.numgate] = tmp;
	q->permuta[k] = tmp;
	}
		p->gp1=base->g;
		p->gp2=guia->g;
		p->tipo = FULL;

		free(q->permuta);
		free (q->vizinho);
		free(q);
		return(p);
}

/* ACMO nao ta completando */
Individuo *CompletaHW ( Individuo * E)
{
Individuo *p;
Uchar i, j, nndef, min, posi, acum, ultimo=0;

p=GeraIndividuoVazio();

for (i=0, nndef=0; i <Matriz.numgate; i++){
				p->permuta[i] = E->permuta[i];
				if (p->permuta[i] == NDEF) {
						nndef++;
						ultimo=i;
				}
		}
/*memcpy(p->permuta, E->permuta, Matriz.numgate);*/
		for (i=1; i <= Matriz.numgate && nndef > 0; i++) /* gera todos os valores de permuta */
		{
				if (NaoPertence(i, p->permuta, Matriz.numgate)==NDEF)
						continue;
		/* se i nao pertence procura um lugar NDEF para ele*/
				for (   j=0, acum=0, min=2*Matriz.numnet;
						j <= ultimo; j++)  {
						if (p->permuta[j] != NDEF ) /* j é a posição de inclusao de i*/
								continue;
		/* quando encontrar acumula os custo de ligar i a vizinhanca*/
						if (nndef < 2) { /* so falta uma posicao */
								posi=j;
								break;
						}
						if (j>=1)
								acum = Custo(i,p->permuta[j-1]);
						if (j<=Matriz.numgate-2)
								acum += Custo(i,p->permuta[j+1]); /* acumula o custo de incluir i em j (j-1, j+1)*/
						if (acum < min ) {
								posi=j;
								min=acum;
								if (min == 0 && rand()%2) break; /* ja encntrou a melhor*/
						}
						else if (acum == min && rand()%2 ) {
								posi=j;
								min=acum;
								if (min == 0) break; /* ja encntrou a melhor*/
						}
				}
				p->permuta[posi]=i;
				nndef--;
}

p->tipo=FULL;
return p;
}

#ifdef FAG

/************** Heurística de Fragiolli *************/

void CalculaFDelta(Individuo * I)
{
	Individuo J;
	int i,j,k;
	unsigned char aux;

	struct {
			int abre, fech, cont, padr;
	} melhor, atual;

	J.permuta = (Byte *) malloc (sizeof(Byte) * Matriz.numgate);
	if (J.permuta == NULL ) { Erro(338); exit(338); }

	memcpy(J.permuta, I->permuta, Matriz.numgate);

	for (i = 0; i < Matriz.numgate - 2; i++){
		if (J.permuta[i] != NDEF && J.permuta[i + 1] != NDEF) {
			melhor.abre = INFINITO;
			melhor.fech = melhor.cont = -1;
			for (j = i + 1; j < Matriz.numgate; j++){
				if (J.permuta[j] != NDEF){
					atual.abre = atual.fech = atual.cont = 0;
					for (k = 0; k < Matriz.numnet; k++) {
						if (Matriz.string[k][J.permuta[i] - 1] == '0' && Matriz.string[k][J.permuta[j] - 1] == '1')
							atual.abre++;
						else if (Matriz.string[k][J.permuta[i] - 1] == '1' && Matriz.string[k][J.permuta[j] - 1] == '0')
							atual.fech++;
						else if (Matriz.string[k][J.permuta[i] - 1] == '1' && Matriz.string[k][J.permuta[j] - 1] == '1')
							atual.cont++;
					}

					if (atual.abre < melhor.abre ||
					    atual.abre == melhor.abre && atual.fech > melhor.fech ||
					    atual.abre == melhor.abre && atual.fech == melhor.fech && atual.cont > melhor.cont ||
					    atual.abre == melhor.abre && atual.fech == melhor.fech && atual.cont == melhor.cont && (rand() % 2)
					){
						melhor = atual;
						melhor.padr = j;
					}
				} // if NDEF j
			} // for j

			aux = J.permuta[melhor.padr];
			J.permuta[melhor.padr] = J.permuta[i + 1];
			J.permuta[i + 1] = aux;
		} // if NDEF i
	} // for i

	CalculaG(&J); // f=g e trk unica para J

	if (J.g < I->g) {
		I->f = J.g;
		I->trilhar = J.trilhas;
		memcpy(I->vizinho, J.permuta, Matriz.numgate);

		/*        I->trilhas = J.trilhas;
				if (I->f < Melhor->f) { // encontrou um melhor que o melhor
				memcpy(I->permuta, J.permuta, Matriz.numgate);
				I->g = J.g;  // g = f p1 = p2 e trk unica
				} */
		// calculo da distancia na permutacao (rank =1 se forem iguais)
		/*        I->rank=1;
				for (i=0; i< Matriz.numgate; i++)
				I->rank += (I->permuta[i] != J.permuta[i]);*/
	} else {
		I->trilhar = I->trilhas;
		memcpy(I->vizinho, I->permuta, Matriz.numgate);
		//RANK QDO G=F
		I->rank = Matriz.numgate + 1;
	}

	free(J.permuta);
	//I->delta =  (1.0F+(I->g - I->f))/I->rank;
	I->delta = (d*(Gmax - I->g) - (I->g - I->f));
	return;
}

#endif

#ifdef OPT

/* ---------- Heurisitca de Treinamento 2Opt -------- */

void CalculaFDeltaNEW(Individuo * I)
{
Individuo J;
int i,j,k,sigmax=0;

J.permuta = (Byte *) malloc (sizeof(Byte) * Matriz.numgate);
if (J.permuta == NULL ) {
		   Erro(337);
		   exit(337);
}

for (i=0, sigmax=0; i<Matriz.numgate-1; i++){
for(j=i+1; j<Matriz.numgate; j++){
		memcpy(J.permuta, I->permuta, Matriz.numgate);
		Opt(&J, i,j);           // movimento 2OPT a partir das referencias
		CalculaG(&J);
// calculo da distancia na permutacao (rank = 1 totalmente igual)
		if ((int)(I->g - J.g) > sigmax) {
				I->f = J.g;
				I->trilhar = J.trilhas;
				memcpy(I->vizinho, J.permuta, Matriz.numgate);
				sigmax = (I->g - J.g);
		}
}
}

//DELTA
if (sigmax == 0){
		I->trilhar = I->trilhas;
		memcpy(I->vizinho, I->permuta, Matriz.numgate);
		I->rank = Matriz.numgate+1;
}
else {
		I->rank=1;
/*        for (k=0; k< Matriz.numgate; k++)
				I->rank += (I->permuta[k] != J.permuta[k]);*/
}

//I->delta =  (1.0F+(I->g - I->f))/I->rank;
 I->delta = (d*(Gmax - I->g) - (I->g - I->f));
free(J.permuta);
return;
}

void CalculaFDelta(Individuo * I)
{
	Individuo J;
	int i, j, k, sigmax = 0;

	J.permuta = (Byte *) malloc (sizeof(Byte) * Matriz.numgate);
	if (J.permuta == NULL ) { Erro(337); exit(337); }

	for (i = 0, sigmax = 0; i < Matriz.numgate-1; i++) {
		for(j = i+1; j < Matriz.numgate; j++) {
			memcpy(J.permuta, I->permuta, Matriz.numgate);
			Opt(&J, i, j); // movimento 2OPT a partir das referencias
			CalculaG(&J);
		
			// calculo da distancia na permutacao (rank = 1 totalmente igual)
			if ((int)(I->g - J.g) > sigmax) {
				I->f = J.g;
				I->trilhar = J.trilhas;
				memcpy(I->vizinho, J.permuta, Matriz.numgate);
				/* I->trilhas = J.trilhas;   coerente f e trilhas
				if (I->f < Melhor->f) { // encontrou um melhor que o melhor
						memcpy(I->permuta, J.permuta, Matriz.numgate);
						I->g = J.g;  // g = f
						I->trilhas = J.trilhas;
				}
				else { */

				//DELTA QDO G <> F
				// calculo da distancia na permutacao
				I->rank=1;
				//for (k=0; k< Matriz.numgate; k++)
				//	I->rank += (I->permuta[k] != J.permuta[k]);

				// I->delta =  (1.0F+(I->g - I->f))/I->rank;
				I->delta = (d*(Gmax - I->g) - (I->g - I->f));
				free(J.permuta);
				return;
			}
		}
	}

	I->trilhar = I->trilhas;
	memcpy(I->vizinho, I->permuta, Matriz.numgate);

	//DELTA QDO G=F
	I->rank = Matriz.numgate+1;
	//I->delta =  (1.0F+(I->g - I->f))/I->rank;
	I->delta = (d*(Gmax - I->g) - (I->g - I->f));
	free(J.permuta);
	return;
}

#endif

void Opt(Individuo * b, int p, int q)
{
	int  i, j;
	Byte *a;
	a = (Byte *) malloc(Matriz.numgate * sizeof(Byte));
	memcpy(a, b->permuta, Matriz.numgate);
	
	if (p+1 == q) {
		j = 0;
		for (i = q; i < Matriz.numgate; i++, j++)
			b->permuta[j] = a[i];
		for (i = 0; i <= p; i++, j++)
			b->permuta[j] = a[i];
	} else {
		j = p+1;
		for (i = q; i >= p+1; i--, j++)
			b->permuta[j] = a[i];
	}

	free (a);
}

int LocalOpt(Individuo * I)
{       int     r = (Uchar) rand() % (Matriz.numgate/2),
				maior = 0, rm,
				atual;
		Individuo *p;
		p=GeraIndividuoVazio();
		memcpy(p->permuta, I->permuta, Matriz.numgate);
		do {
				atual=Custo(p->permuta[r],p->permuta[r+1]);
				if (atual > maior){
						rm = r;
						maior = atual;
				}
				r ++;
		}while(r < Matriz.numgate-1);
		Opt(p, rm, rm+1);
		CalculaG(p);
		if (p->g <= I->g) {
				memcpy(I->permuta,p->permuta, Matriz.numgate);
				I->g = p->g;
				free (p->permuta);
				free (p->vizinho);
				free(p);
				return (1);
		}
		free(p->permuta);
		free(p->vizinho);
		free(p);
		return(0);
}

Individuo * RecombinaPE(Individuo *base, Individuo *guia)
{
		int     r,
				 atual,  in1, fi1, in2, fi2;
		Individuo *filho;

		filho=GeraIndividuoVazio();
/*        memcpy(filho->permuta, base->permuta, Matriz.numgate);*/
		r = (Uchar) rand() % (Matriz.numgate/2);
		do {
				 r++;
				 atual=Custo(base->permuta[r],base->permuta[r+1]);
		}while( atual < FATOR*Matriz.numnet && r < Matriz.numgate-1);
		in1=r;
		r++;
		while( atual >= FATOR*Matriz.numnet && r < Matriz.numgate-1) {
				 atual=Custo(base->permuta[r],base->permuta[r+1]);
				 r++;
		}
		fi1=r;
		r = (Uchar) rand() % (Matriz.numgate/2);
		do {
				 r++;
				 atual=Custo(guia->permuta[r],guia->permuta[r+1]);
		}while( atual < FATOR*Matriz.numnet && r < Matriz.numgate-1);
		in2=r;
		r++;
		while( atual >= FATOR*Matriz.numnet && r < Matriz.numgate-1){
				 atual=Custo(guia->permuta[r],guia->permuta[r+1]);
				 r++;
		}
		fi2=r;

		if (fi1-in1 <=1) in1 +=2; /* desabilita */
		if (fi2-in2 <=1) in2 +=2; /* desabilita */
		filho->tipo = FULL;
		for (r=0; r< Matriz.numgate; r++)
		{//ACMO intervalos ?????
				if ( !(r > in1 && r <= fi1) ) {
						filho->permuta[r] = NaoPertence(base->permuta[r], filho->permuta, r);
						if ( !(r >= in2 && r <= fi2) && filho->permuta[r] == NDEF)
								filho->permuta[r] = NaoPertence(guia->permuta[r], filho->permuta, r);
				}
				else {
				if ( !(r > in2 && r <= fi2) )
						filho->permuta[r] = NaoPertence(guia->permuta[r], filho->permuta, r);
				else
						filho->permuta[r] = NDEF;
				}
		if (filho->permuta[r] == NDEF)
				filho->tipo=!FULL;
		}
		return filho;
}

void Esquematiza(Individuo *base)
{
	int     r,
		atual, ini, fim;
	r = (Uchar) rand() % (Matriz.numgate / 2);
	do {
		r++;
		atual = Custo(base->permuta[r], base->permuta[r + 1]);
	} while (atual < FATOR*Matriz.numnet && r < Matriz.numgate - 1);
	ini = r;
	r++;
	while (atual >= FATOR*Matriz.numnet && r < Matriz.numgate - 1) {
		atual = Custo(base->permuta[r], base->permuta[r + 1]);
		r++;
	}
	fim = r;

	if (fim - ini <= 1) ini += 2; /* desabilita */

	for (r = 0; r< Matriz.numgate; r++)
	{
		if (r > ini && r <= fim) {
			base->permuta[r] = NDEF;
		}
		if (base->permuta[r] == NDEF)
			base->tipo = !FULL;
	}
}

Individuo *GeraIndividuoVazio()
{
	Individuo * p;
	p = (Individuo *) malloc(sizeof(Individuo));
	if (!p) { Erro(200); exit(200); }

	p->permuta = (Uchar *) malloc(sizeof(Uchar) * Matriz.numgate);
	if (!(p->permuta)) { Erro(201); exit(201); }

	p->vizinho = (Uchar *) malloc(sizeof(Uchar) * Matriz.numgate);
	if (!(p->vizinho)) { Erro(201); exit(201); }

	p->f = 0;
	p->g = 0;
	p->sel = 0;
	p->mut = 0;
	p->col = 0;
	p->gp1 = 0;
	p->gp2 = 0;
	p->alf = 0;
	p->delta = 0;
	p->trilhas = 0;
	p->trilhar = 0;
	p->rank = 0;
	p->tipo = 0;
	p->geracao = 0;

	return p;
}

Individuo *GeraIndividuoIgual(Individuo *I)
{
	Individuo * p;
	int i;
	p = GeraIndividuoVazio();
	
	for (i = 0; i < Matriz.numgate; i++) {
		p->permuta[i] = I->permuta[i];
		p->vizinho[i] = I->vizinho[i];
	}

	p->f       = I->f;
	p->g       = I->g;
	p->sel     = I->sel;
	p->mut     = I->mut;
	p->col     = I->col;
	p->gp1     = I->gp1;
	p->gp2     = I->gp2;
	p->alf     = I->alf;
	p->delta   = I->delta;
	p->trilhas = I->trilhas;
	p->trilhar = I->trilhar;
	p->rank    = I->rank;
	p->tipo    = I->tipo;
	p->geracao = I->geracao;

	return p;
}



