#include <stdlib.h>
#include <string.h>
#include "parse_opt.h"
#include "problem.h"
#include "dbg.h"

// parameter parsing configuration
const char *argp_program_version = "ptamosp 1.1a - Thiago de Sousa Pereira";
const char *argp_program_bug_address = "<sthiago@gmail.com>";

/* Program documentation. */
char doc[] = "PTA MOSP/VLSI problem optimizer -- a program to find (near-)optimal solutions to MOSP and VLSI problems";

/* A description of the arguments we accept. */
char args_doc[] = "PROBLEM";

/* The options we understand. */
struct argp_option options[] = {
	{ 0, 0, 0, 0, "Execution options:", 1 },
	{ "max-generations", 'g', "NUM", 0, "Maximum number of generations/loops (Default: 100)", 0 },
	{ "amostra", 251, "NUM", 0, "Interval of generations to show best value (verbose must be on) (Default: 5)", 0 },
	{ "cfo", 'c', "NUM", 0, "Maximum number of calls to the objective function (Default: 200)", 0 },
	{ "verbose", 'v', 0, 0, "Enable verbose output", 0 },
	{ "output", 'o', "FILE", 0, "Output to FILE. Default is the input file with .csv extension", 0 },
	{ "initial-population", 'p', "NUM", 0, "Number of individuals in initial population (Default: 50)", 0 },
	{ "num-crossovers", 'x', "NUM", 0, "Number of crossovers. Suggested to be initial-population/10 (Default: 5)", 0 },
	{ "num-steps", 's', "NUM", 0, "Number of mutation steps (Default: 20)", 0 },
	{ "perc-mutations", 'm', "PERC", 0, "Percentage of mutations (Default: 1.0)", 0 },
	{ "perc-best", 'b', "PERC", 0, "Percentage of best (??) (Default: .20)", 0 },
	{ "deviation", 'd', "PERC", 0, "?? (Default: .005)", 0 },
	{ "alpha", 'a', "PERC", 0, "Alpha correction factor (pop. trimming) (Default: .005)", 0 },
	{ "xls-output", 255, 0, 0, "Enable CSV-like output", 0 },
	{ "random-seed", 253, "SEED", OPTION_HIDDEN, "Set the random seed to SEED (Default: time(0))", 0 },
	{ "iteracao", 'i', "NUM", OPTION_HIDDEN, "Iteracao (para output de relatorio)", 0 },

	{ 0, 0, 0, 0, "Problem-related options:", 0 },
	{ "problem-type", 't', "TYPE", 0, "Problem type can be either MOSP or VLSI (Default: MOSP)", 0 },
	{ "heuristic", 'h', "TYPE", 0, "Training heuristic can be FAG (Fagiolli), OPT (2-Opt), COOP (both, cooperatively), COMP (both, competitively), PARALLEL (Circle-like). (Default: COMP)", 0 },
	{ "fator", 'f', "NUM", 0,  "?? (Default: 20)", 0},
	{ "largo", 'l', "NUM", 0, "?? (Default: 20)", 0 },
	{ "min-pop", 252, "NUM", 0, "Minimum population (Default: 10)", 0 },
	{ "solution", 254, "NUM", 0, "The solution to the problem. The algorithm will stop when it is found", 0 },
	{ 0, 0, 0, 0, 0, 0 }
};

/* Parse a single option. */
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
	/* Get the INPUT argument from `argp_parse', which we
	know is a pointer to our arguments structure. */
	struct arguments *args = state->input;

	switch (key) {
	case 'g': args->max_loop = atoi(arg); break;
	case 251: args->amostra = atoi(arg); break;
	case 'c': args->max_cfo = atoi(arg); break;
	case 'v': args->verbose = TRUE; break;
	case 'o': args->output_file = arg; break;
	case 'p': args->init_pop = atoi(arg); break;
	case 'x': args->n_cross = atoi(arg); break;
	case 's': args->n_steps = atoi(arg); break;
	case 'm': args->perc_mutation = atof(arg); break;
	case 'd': args->deviation = atof(arg); break;
	case 'b': args->perc_best = atof(arg); break;
	case 255: args->xls = TRUE; break;
	case 254: args->solution = atoi(arg); break;
	case 253: args->seed = atoi(arg); break;
	case 252: args->min_pop = atoi(arg); break;
	case 'l': args->largo = atoi(arg); break;
	case 'f': args->fator = atoi(arg); break;
	case 'i': args->iteracao = strdup(arg); break;
	case 'a': args->alfa1 = atof(arg); break;
	
	case 't':
		args->type = parse_prob_type(arg);
		args->type_name = strdup(arg);
		break;

	case 'h':
		args->heur = parse_heur_name(arg);
		args->heur_name = strdup(arg);
		break;


	case ARGP_KEY_ARG:
		/* Too many arguments. */
		if (state->arg_num > 1)
			argp_usage(state);

		args->args[state->arg_num] = arg;

		break;

	case ARGP_KEY_END:
		/* Not enough arguments. */
		if (state->arg_num < 1)
			argp_usage(state);
		break;

	default:
		return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

void set_default_params(struct arguments *args)
{
	args->max_loop = 100;
	args->amostra = 5;
	args->max_cfo = 500000;
	args->verbose = FALSE;
	args->xls = FALSE;
	args->init_pop = 50;
	args->n_cross = 5;
	args->n_steps = 20;
	args->perc_mutation = 1.0f;
	args->perc_schemas = 0.0f;
	args->perc_best = .20f;
	args->deviation = .005f;
	args->output_file = "-";
	args->type = MOSP;
	args->type_name = strdup("MOSP");
	args->heur = COMP;
	args->heur_name = strdup("COMP");
	args->largo = 20;
	args->fator = 20;
	args->min_pop = 10;
	args->seed = (unsigned int) time(0);
	args->solution = -1;
	args->alfa1 = .005f;
	args->iteracao = strdup("X");
}

char *get_default_output(const char *input_file)
{
	char *output_file = NULL;
	check(input_file, "%s", "Invalid input file name");

	// length of input + 1 (\0) + 4 (".agc");
	output_file = malloc(strlen(input_file) + 1 + 4);
	check_mem(output_file);

	strcpy(output_file, input_file);
	strcat(output_file, ".csv");

	return output_file;

error:
	exit(-1);
}

enum heur_t parse_heur_name(const char *option)
{
	enum heur_t choice = -1;
	if (strcmp(option, "FAG") == 0) {
		choice = FAG;
	} else if (strcmp(option, "OPT") == 0) {
		choice = OPT;
	} else if (strcmp(option, "COMP") == 0) {
		choice = COMP;
	} else if (strcmp(option, "COOP") == 0) {
		choice = COOP;
	} else if (strcmp(option, "COOP_E") == 0) {
		choice = COOP_E;
	} else if (strcmp(option, "PARALLEL") == 0) {
		choice = PARALLEL;
	} else {
		die("unknown heuristic option: %s", option);
	}

	return choice;
}

enum prob_t parse_prob_type(const char *option)
{
	enum prob_t choice = -1;
	if (strcmp(option, "MOSP") == 0) {
		choice = MOSP;
	} else if (strcmp(option, "VLSI") == 0) {
		choice = VLSI;
	} else {
		die("unknown problem type: %s", option);
	}

	return choice;
}

/* our argp parser. */
struct argp argp = { options, parse_opt, args_doc, doc, 0, 0, 0 };