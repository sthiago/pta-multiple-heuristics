#include <stdlib.h>
#include "dbg.h"
#include "problem.h"
#include "population.h"
#include "individuo.h"

void init_population(problem *p)
{
	while (p->pop->tamanho < p->args.init_pop) {
		individuo *i = cria_individuo(p, SCHEMA);

		init_individuo(i);
		f_objetivo(i);

		// atualiza gmax
		if (i->g >= p->gmax) p->gmax = 2 * (i->g + 1);

		f_heuristica(i);
		f_delta(i);

		if (i->f < p->best->f) {  // aqui g = f
			// destroi_individuo(p->best);
			// p->best = cria_individuo(p, FULL);
			copia_individuo(p->best, i);
		}

		i->geracao = 1; // gera��o inicial
		insere_individuo(p->pop, i);
	}
}

/* Insere na popula��o ordenando pelo ranking do individuo */
void insere_individuo(population *p, individuo *i)
{
	double chave = i->delta;
	int nGates = i->prob->n_gates;

	elemento *E = NULL, *aux = NULL;
	uchar iguais = FALSE;

	E = malloc(sizeof(*E));
	check_mem(E);

	E->indi = i;
	E->prox = NULL;
	E->ante = NULL;

	// se a popula��o tava vazia
	if (p->inicio == NULL) {
		p->inicio = p->fim = E;
		p->tamanho++;
	} else {

		// navega pela popula��o at� que chave >= delta
		for (aux = p->inicio; aux != NULL && chave < aux->indi->delta; aux = aux->prox)
			;

		// navega pelos pr�ximos elementos caso chave == delta.
		for (; aux != NULL && chave == aux->indi->delta; aux = aux->prox) {
			if (memcmp(E->indi->permuta, aux->indi->permuta, nGates) == 0) {
				E->indi->col++;
				aux->indi->col++;
				iguais = TRUE;
				break;
			}
		}

		// se n�o encontrou nenhum individuo igual, adiciona � popula��o
		//if (iguais) log_info("%s", "Iguais");
		//if (!iguais) log_info("%s", "Diferentes");

		if (!iguais) {
			// se nao chegou ao fim da populacao, insere no meio
			if (aux) {
				E->prox = aux;
				E->ante = aux->ante;
				aux->ante = E;

				if (E->ante == NULL)
					p->inicio = E;
				else
					E->ante->prox = E;

			// se chegou ao fim, insere no fim
			} else {
				E->prox = NULL;
				E->ante = p->fim;
				p->fim = E;
				E->ante->prox = E;
			}

			p->tamanho++;

		// se j� tinha algum indiv�duo igual, n�o adiciona � popula��o
		} else {
			destroi_individuo(E->indi);
			free(E);
		}
	}

	return;
}

individuo *seleciona(population *pop, enum indi_t type)
{
	problem *p = pop->inicio->indi->prob; // hack

	float percent = 100.0;
	if (type == BASE) percent = pop->tamanho * p->args.perc_best;
	if (type == GUIA) percent = pop->tamanho;

	uchar escolha = (uchar) rand() % (int) (percent + 1);

	elemento *elem;
	for (elem = pop->inicio; elem != pop->fim && escolha; elem = elem->prox)
		escolha--;

	elem->indi->sel++;

	return elem->indi;
}

individuo *seleciona_base(population *pop) { return seleciona(pop, BASE); }
individuo *seleciona_guia(population *pop) { return seleciona(pop, GUIA); }

int mutacao(individuo *ind)
{
	problem *p = ind->prob;


	// configura��o de indiv�duos auxiliares
	individuo J, K;
	J.prob = K.prob = ind->prob;

	J.permuta = malloc(sizeof(uchar) * p->n_gates);
	check_mem(J.permuta);

	K.permuta = malloc(sizeof(uchar) * p->n_gates);
	check_mem(K.permuta);

	// o que s�o y e z?
	uchar y = (p->n_gates <= p->args.largo ? 0 : rand() % (p->n_gates - p->args.largo));
	uchar z = y + (p->n_gates <= p->args.largo ? p->n_gates : p->args.largo);

	int sigmax = 0;
	for (int i = y; i < z; i++) {
		for (int j = i + 1; j < z; j++) {
			memcpy(J.permuta, ind->permuta, p->n_gates);
			opt_movement(&J, i, j);
			f_objetivo(&J);

			if (ind->g - J.g > sigmax) {
				memcpy(K.permuta, J.permuta, p->n_gates);
				K.g = J.g;
				K.trilhas = J.trilhas;
				K.tipo = J.tipo;
				//K.rank = J.rank;
				sigmax = ind->g - J.g;
			}
		}
	}

	if (sigmax > 0) {
		memcpy(ind->permuta, K.permuta, p->n_gates);
		ind->g = ind->f = K.g;
		ind->trilhas = K.trilhas;
		ind->tipo = K.tipo;
		//ind->rank = K.rank; /* temporariamente ranking � ndefs*/
		ind->mut++;

		free(J.permuta);
		free(K.permuta);

		return TRUE;
	}

	free(J.permuta);
	free(K.permuta);

	return FALSE;
}

individuo *recombina_BO(individuo *base, individuo *guia)
{
	if (base->tipo != FULL || guia->tipo != FULL)
		return NULL;

	int nGates = base->prob->n_gates;

	individuo *p = cria_individuo(base->prob, FULL);
	uchar *q = malloc(sizeof(uchar) * nGates);
	check_mem(q);

	int cont = nGates;
	int k = nGates / 2;

	for (int i = 0; i < nGates; i++)
		p->permuta[i] = q[i] = 0;

	for (int i = 0; i <= k; i++) {
		int j;
		if ((j = rand() % nGates)) {
			p->permuta[j] = base->permuta[j];
			cont -= !q[p->permuta[j] - 1];
			q[p->permuta[j] - 1] = 1;
		}
	}

	for (int i = 0, j = 0; i < nGates && cont; i++) {
		if (!p->permuta[i]) {
			while (q[guia->permuta[j++] - 1])
				;
			p->permuta[i] = guia->permuta[j - 1];
			cont--;
		}
	}

	//p->gp1 = base->g;
	//p->gp2 = guia->g;
	p->tipo = FULL; // precisa setar porque normalmente s� seta quando init_individuo

	free(q);
	return p;
}

individuo *poda_populacao(population *p, double alfa)
{
	elemento *aux;
	individuo *i = NULL;

	if (p->fim == NULL) {
		return i;
	} else {
		aux = p->fim;
		do {
			if (aux->indi->delta <= alfa) {

				aux = aux->ante;
				p->fim = aux;

				if (aux != NULL) {

					if (aux->prox->indi->f == aux->prox->indi->g) {

						destroi_individuo(aux->prox->indi);
						free(aux->prox);

						aux->prox = NULL;
						p->tamanho--;

					} else {
						i = aux->prox->indi; //salva indi

						free(aux->prox);

						aux->prox = NULL;
						p->tamanho--;       // delimita e exclui
						break;
					}
				}
			}
		} while (aux != NULL && aux->indi->delta <= alfa);

		if (aux == NULL) {
			free(p->inicio);
			p->inicio = p->fim = NULL;
		}
	}

	return i;
}