#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <float.h>
#include <mpi.h>
#include "dbg.h"
#include "parse_opt.h"
#include "problem.h"
#include "population.h"
#include "individuo.h"

#define MOD(a,b) ((((a)%(b))+(b))%(b))

enum end_t { SOLUTION = 1, MAXLOOP, MINPOP, FORCED, MAXCFO };
enum msg_t { BEST, EXITING, BEST_G, BEST_TRILHAR, BEST_TRILHA2 };

int main(int argc, char **argv)
{
	clock_t start, end;
	MPI_Init(&argc, &argv);
	problem *p = init_problem(argc, argv);

	MPI_Datatype ind_permuta;
	MPI_Type_contiguous(p->n_gates, MPI_CHAR, &ind_permuta);
	MPI_Type_commit(&ind_permuta);

	// initial population
	if (p->args.verbose) log_info("%s", "generating initial population");
	init_population(p);

	// evolution setup
	if (p->args.verbose) log_info("%s", "natural evolution");
	time_t t0 = time(0);
	if (p->heur != PARALLEL || p->world_rank == 0) { start = clock(); }
	p->generation = 0;
	enum end_t fim = p->args.amostra; // ??
	double alfa = p->pop->fim->indi->delta; // n�o � o mesmo p->args.alfa1, � o limiar de rejei��o

	// main loop
	individuo *novo;
	if (p->args.verbose) log_info("%s", "starting main loop\n");
	while (TRUE) {

		// condi��es de parada
		if (p->pop->tamanho <= p->args.min_pop) { fim = MINPOP; break; }
		if (fabsf(p->best->trilhar - p->args.solution) < 0.001) { fim = SOLUTION;  break; }
		if (p->args.max_loop > 0 && p->generation >= p->args.max_loop) { fim = MAXLOOP; break; }
		if (p->args.max_cfo > 0 && p->n_cfo >= p->args.max_cfo) { fim = MAXCFO; break; }

		fflush(stdout);
		fflush(stderr);

		// verifica se o �timo j� foi encontrado
		// (caso p->args.solution > 0)
		if (p->heur == PARALLEL && p->args.solution > 0) {
			MPI_Status status;
			int msg = FALSE, should_exit = FALSE;
			int src = MOD(p->world_rank + 1, p->world_size);
			MPI_Iprobe(src, EXITING, MPI_COMM_WORLD, &msg, &status);
			
			if (msg) {
				// recebe a msg, pro src n�o ficar travado
				MPI_Recv(&should_exit, 1, MPI_INT, src, EXITING, MPI_COMM_WORLD, &status);
				// should_exit = TRUE;

				// sai do loop (ao sair, ir� propagar a mensagem ao anterior.
				if (should_exit) {
					fim = FORCED; // algu�m mandou eu sair
					break;
				}
			}
		}


		if (p->generation > 0 && p->heur == PARALLEL && p->world_size > 1) {
			MPI_Status status;
			int message;

			// recebe do processo anterior (circular)
			int src = MOD(p->world_rank - 1, p->world_size);

			MPI_Iprobe(src, BEST, MPI_COMM_WORLD, &message, &status);

			if (message) {
				char *recv_permuta = malloc(p->n_gates * sizeof(*recv_permuta));

				MPI_Recv(recv_permuta, 1, ind_permuta, src, BEST, MPI_COMM_WORLD, &status);

				// cria individuo com essa permuta
				individuo *recv_ind = cria_individuo(p, FULL);
				memcpy(recv_ind->permuta, recv_permuta, p->n_gates);
				free(recv_permuta);

				f_objetivo(recv_ind);
				f_heuristica(recv_ind);
				f_delta(recv_ind);

				insere_individuo(p->pop, recv_ind);
			}

		}

		p->generation++;
		fim--;

		// realiza args.n_cross cruzamentos
		for (int i = 0; i < p->args.n_cross; i++) {

			// sele��o
			individuo *base = seleciona_base(p->pop);
			individuo *guia = seleciona_guia(p->pop);

			// chance de realizar muta��o
			int chance = rand();
			if ((chance % 100) < (int) (p->args.perc_mutation * 100)) {
				individuo *novo_c = cria_individuo(p, FULL);
				copia_individuo(novo_c, base);

				// realize at� args.n_steps muta��es
				for (int j = 0; j < p->args.n_steps; j++) {
					int rv = mutacao(novo_c);
					if (rv == FALSE)
						break;
				}

				novo_c->geracao = p->generation;
				//novo_c->alf = alfa;
				f_heuristica(novo_c);
				f_delta(novo_c);

				// encontrou um melhor ap�s MUTA��O
				if (novo_c->f < p->best->f) {  // aqui g = f
					fim++;
					p->new_best = TRUE;

					//destroi_individuo(p->best);
					copia_individuo(p->best, novo_c);

					printf("[%d] +(B) Ger=%d, Pop=%d, a= %.4f, p=%d, f=%d, ** %d(%d) **, %.4f\n",
						   p->world_rank,
					       p->generation,
					       p->pop->tamanho,
					       alfa,
						   p->args.n_steps,
					       (int) p->best->f,
					       (int) (p->best->f - (p->n_gates * p->n_nets * p->best->trilhar)),
					       (int) p->best->trilhar,
					       p->best->delta
					);
				}

				if (alfa < novo_c->delta) {
					insere_individuo(p->pop, novo_c);
				} else {
					destroi_individuo(novo_c);
				}
			}

			if (base == guia)
				continue;

			novo = recombina_BO(base, guia);
			novo->geracao = p->generation;
			
			//Novo->alf = alfa;
			f_objetivo(novo);
			f_heuristica(novo);
			f_delta(novo);

			// encontrou um melhor ap�s RECOMBINA��O
			if (novo->f < p->best->f) {  // aqui g = f
				p->new_best = TRUE;
				fim++;

				//destroi_individuo(p->best);
				copia_individuo(p->best, novo);

				printf("[%d] +(N) Ger=%d, Pop=%d, a= %.4f, p=%d, f=%d, ** %d(%d) **, %.4f\n",
					   p->world_rank,
				       p->generation,
				       p->pop->tamanho,
				       alfa,
					   p->args.n_steps,
				       (int) p->best->f,
				       (int) (p->best->f - (p->n_gates * p->n_nets* p->best->trilhar)),
				       (int) p->best->trilhar,
				       p->best->delta
				);
			}

			if (alfa < novo->delta) {
				insere_individuo(p->pop, novo);
			} else {
				destroi_individuo(novo);
			}

		}

		// incremento do limiar de rejei��o
		float delta_melhor = p->pop->inicio->indi->delta;
		float delta_pior = p->pop->fim->indi->delta;
		alfa += p->args.alfa1 * p->pop->tamanho * (delta_melhor - delta_pior) / (p->args.max_loop - p->generation);
		
		// avalia��o em rela��o ao limiar
		while (alfa >= p->pop->fim->indi->delta && p->pop->tamanho)
		{
			novo = poda_populacao(p->pop, alfa);
			if (novo == NULL) break;

			memcpy(novo->permuta, novo->vizinho, p->n_gates);

			novo->g = novo->f;
			novo->trilhas = novo->trilhar;

			f_heuristica(novo);
			f_delta(novo);

			// encontrou um melhor ap�s tentativa de remov�-lo (reinsere na popula��o)
			if (novo->f < p->best->f) {  // aqui g = f
				p->new_best = TRUE;
				fim++;

				//destroi_individuo(p->best);
				copia_individuo(p->best, novo);

				printf("[%d] +(P) Ger=%d, Pop=%d, a= %.4f, p=%d, f=%d, ** %d(%d) **, %.4f\n",
					   p->world_rank,
				       p->generation,
				       p->pop->tamanho,
				       alfa,
				       p->args.n_steps,
				       (int) p->best->f,
				       (int) (p->best->f - (p->n_gates * p->n_nets * p->best->trilhar)),
				       (int) p->best->trilhar,
				       p->best->delta
				);
			}

			if (alfa < novo->delta) {
				insere_individuo(p->pop, novo);
			} else {
				destroi_individuo(novo);
			}
		}

		// Se um novo melhor (new_best) foi encontrado nessa gera��o, envia
		if (p->new_best && p->heur == PARALLEL && p->world_size > 1) {
			p->new_best = FALSE;

			// envia para o pr�ximo processo (circular)
			int dest = (p->world_rank + 1) % p->world_size;
			MPI_Send(p->best->permuta, 1, ind_permuta, dest, BEST, MPI_COMM_WORLD);
		}

		if (p->args.verbose) {
			//printf("Ger = %d, CFO = %d\n", evolution->generation, p->nCFO);
			if (!(p->generation % p->args.amostra))
				printf("[%d] #### Ger=%d, Pop=%d, a= %.4f, %d-%d, ** %d(%d) **\n",
				p->world_rank,
				p->generation,
				p->pop->tamanho,
				alfa,
				(int) p->best->f,
				(int) p->best->g,
				(int) (p->best->f - (p->n_gates * p->n_nets * p->best->trilhar)),
				(int) p->best->trilhar
				);
		}
	}

	fflush(stdout);
	fflush(stderr);

	// /////////////////////////////
	// RESULTADOS
	///////////////////////////////

	time_t t1 = time(0);
	if (p->heur != PARALLEL || p->world_rank == 0) { end = clock(); }
	
	if (p->args.verbose) {
		// qual motivo levou o algoritmo a terminar
		log_info("finalizando por:");
		switch (fim) {
			case SOLUTION: log_info("> prob. solucao: %f", p->best->trilhar); break;
			case MAXLOOP:  log_info("> geracao: %d", p->generation); break;
			case MINPOP:   log_info("> populacao: %d", p->pop->tamanho); break;
			case FORCED:   log_info("> forced"); break;
			case MAXCFO:   log_info("> max. num. chamadas F.O.: %d", p->n_cfo); break;
			default:       log_info("> cond. termino: %d", fim);
		}
	}

	if (p->heur == PARALLEL) {
		printf("[%d] exited main loop (%d)\n", p->world_rank, fim);
	}

	// avisa o cara anterior que eu sa� do loop
	if (p->heur == PARALLEL && p->world_size > 1 && (fim == SOLUTION) || (fim == FORCED)) {
		int dest = MOD(p->world_rank - 1, p->world_size);
		int should_exit = TRUE;
		MPI_Request req;
		MPI_Isend(&should_exit, 1, MPI_INT, dest, EXITING, MPI_COMM_WORLD, &req);
	}

	// RESULTADO MPI (WR0 recebe o p->best de todo mundo e imprime o melhor)
	if (p->heur == PARALLEL && p->world_size > 1) {

		float trilha2 = (p->best->f - (p->n_gates * p->n_nets * p->best->trilhar));
		float *best_g, *best_trilhar, *best_trilha2;

		if (p->world_rank == 0) {
			best_g = malloc(p->world_size * sizeof(*best_g));
			best_trilhar = malloc(p->world_size * sizeof(*best_trilhar));
			best_trilha2 = malloc(p->world_size * sizeof(*best_trilha2));

			best_g[0] = p->best->g;
			best_trilhar[0] = p->best->trilhar;
			best_trilha2[0] = trilha2;
			
			// printf("[%d] \"enviei\": %d %d %d\n", p->world_rank, (int) p->best->trilhar, (int) trilha2, (int) p->best->g);

			for (int i = 1; i < p->world_size; i++) {
				MPI_Status status;
				MPI_Recv(&best_g[i], 1, MPI_FLOAT, i, BEST_G, MPI_COMM_WORLD, &status);
				MPI_Recv(&best_trilhar[i], 1, MPI_FLOAT, i, BEST_TRILHAR, MPI_COMM_WORLD, &status);
				MPI_Recv(&best_trilha2[i], 1, MPI_FLOAT, i, BEST_TRILHA2, MPI_COMM_WORLD, &status);

				// printf("recebi: %d: %d %d %d\n", i, (int) best_trilhar[i], (int) best_trilha2[i], (int) best_g[i]);
			}
		} else {
			MPI_Status status;
			MPI_Send(&p->best->g, 1, MPI_FLOAT, 0, BEST_G, MPI_COMM_WORLD);
			MPI_Send(&p->best->trilhar, 1, MPI_FLOAT, 0, BEST_TRILHAR, MPI_COMM_WORLD);
			MPI_Send(&trilha2, 1, MPI_FLOAT, 0, BEST_TRILHA2, MPI_COMM_WORLD);

			// printf("[%d] enviei: %d %d %d\n",p->world_rank, (int) p->best->trilhar, (int) trilha2, (int) p->best->g);
		}

		if (p->world_rank == 0) {

			//printf("1. best_trilhar[]: ");
			//for (int i = 0; i < p->world_size; i++)
			//	printf("%d, ", (int) best_trilhar[i]);
			//printf("\n");

			// encontrar o melhor dentre os valores recebidos
			float min_trilhar = FLT_MAX;
			//printf("2. min_trilhar = %f\n", min_trilhar);
			for (int i = 0; i < p->world_size; i++) {
				if (best_trilhar[i] < min_trilhar) {
					min_trilhar = best_trilhar[i];
				} 
			}
			//printf("3. min_trilhar = %d\n", (int) min_trilhar);

			for (int i = 0; i < p->world_size; i++) {
				if ((int) best_trilhar[i] > (int) min_trilhar) {
					best_trilhar[i] = -1;
				}
			}

			//printf("4. best_trilhar[]: ");
			//for (int i = 0; i < p->world_size; i++)
			//	printf("%d, ", (int) best_trilhar[i]);
			//printf("\n");

			float min_trilha2 = FLT_MAX;
			int min_index = -1;
			for (int i = 0; i < p->world_size; i++) {
				if (best_trilhar[i] > 0 && best_trilha2[i] < min_trilha2) {
					min_trilha2 = best_trilha2[i];
					min_index = i;
				}
			}

			double cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
			log_info("BEST [%d]: %d -- %d(%d) -- %.3fs\n", min_index, (int) best_g[min_index], (int) min_trilha2, (int) min_trilhar, cpu_time_used);

			FILE *relatorio = fopen("relatorio.txt", "a");
			fprintf(relatorio, "%s\t%d\t%d\t%d\t%d\t%.3f\n", p->args.args[0], p->world_size, (int) best_g[min_index], (int) min_trilha2, (int) min_trilhar, cpu_time_used);
			fclose(relatorio);
		}
	}

	if (p->heur != PARALLEL) {
		double cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

		FILE *relatorio = fopen("relatorio.txt", "a");
		fprintf(relatorio, "%s\t%s\t%d\t%d\t%.3f\n", p->args.args[0], p->args.heur_name, (int) p->best->g, (int) p->best->trilhar, cpu_time_used);
		fclose(relatorio);
	}

	// #ifdef EXTRA
	// MutacaoTotal(Melhor);
	// #endif

	if (p->args.xls) {
		fprintf(p->log_file, "\n%s,%s,", p->args.type_name, p->args.output_file);
		fprintf(p->log_file, "%d,%d,%d,%d,%u,%u",
			p->generation,
			(int) (t1 - t0),
			(int) (p->best->f - (p->n_gates * p->n_nets * p->best->trilhar)),
			(int) p->best->trilhar,
			p->best->geracao,
			p->n_cfo
		);

		// fprintf(log_file, " %.4f; %d;",alfa, P.tamanho);
	} else {
		// fprintf(ars, "\n%s%d: %s", MOSP, Geracao, st);
		// fprintf(ars, "\n\n\n %s ", MOSP);
		// fprintf(ars, "\n          Alexandre C�sar Oliveira - 2000");
		// fprintf(ars, "\n          Gate minimization\n");
		// fprintf(ars, "\n          Entrada: %d, %.4f, %.4f, %.4f, %s", MAXLOOP, VARALF1, VARALF2, d, argv[5]);
		// fprintf(ars, "\n          Matriz...:");
		//
		// for (j = 0; j < p->nGates; j++) {
		// 	fprintf(ars,"\n\r");
		// 	for (i = 0; i < p->nNets; i++)
		// 		fprintf (ars, " %c ", Matriz.string[i][j]);
		// }
		//
		// fprintf(ars, "\n          Tempo: %ul a %ul = %ul", t0, t1, t1-t0);
		// fprintf(ars, "\n          Ger: %d(%.4f), Pop: %d, Gmax: %.4f, Aval: %u", Geracao, alfa, P.tamanho, Gmax, P.naval);
		// fprintf(ars, "\n          Melhores...\n");
		//
		// MostraInd(ars, Melhor);
		//
		// #endif
	}

	if (p->args.verbose) {
		printf("\n### FINAL Ger = %d, Pop = %d, ** %d(%d) **\n",
		       p->generation,
		       p->pop->tamanho,
		       (int) (p->best->f - (p->n_gates * p->n_nets * p->best->trilhar)),
		       (int) p->best->trilhar
		       );
	}

	// #ifdef DUMP
	// fprintf(ars, "\n          Populacao...\n");
	// MostraPop(ars, P);
	// #endif
	//
	// if (P.tamanho > 1)
	// 	PodaPopulacao(&P, P.inicio->indi->delta + 1.0F);
	
	MPI_Finalize();

	return 0;
	
error:

	return -1;
}



