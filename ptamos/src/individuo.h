#ifndef _individuo_h_
#define _individuo_h_

enum indi_t { SCHEMA, FULL, GUIA, BASE };
enum info_t { NDEF, DEF };

typedef struct _individuo individuo;
typedef struct _problem problem;

typedef void(*initialize_strategy)(individuo *ind);
typedef float(*heuristics_strategy)(individuo *ind);
typedef float(*delta_strategy)(individuo *ind);

struct _individuo {
	unsigned char *permuta, *vizinho;
	float f_fag, f_opt, f, g, delta;
	float trilhas, trilhar;
	int geracao;
	unsigned sel, mut, col; // ??? o que s�o?

	problem *prob;
	enum indi_t tipo;
	initialize_strategy initialize;
	heuristics_strategy f_heuristics;
	delta_strategy f_delta;
};

individuo *cria_individuo(problem *p, enum indi_t type);
void init_individuo(individuo *ind);
void destroi_individuo(individuo *ind);
void copia_individuo(individuo *dest, individuo *src);

float f_objetivo(individuo *ind);
float f_heuristica(individuo *ind);
float f_delta(individuo *ind);

void opt_movement(individuo *ind, int p, int q);

void show_permuta(FILE *stream, individuo *ind);

#endif