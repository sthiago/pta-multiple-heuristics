#include <stdlib.h>
#include <limits.h>
#include <math.h>

#include "dbg.h"
#include "individuo.h"
#include "problem.h"
#include "population.h"

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

static void set_initialize_strategy(individuo *ind, enum indi_t type);
static void set_heuristics_strategy(individuo *ind, enum heur_t heur);

////////////////////////////////////////
// cria��o e manuten��o

individuo *cria_individuo(problem *p, enum indi_t type)
{
	individuo *i = calloc(1, sizeof(*i));
	check_mem(i);

	i->permuta = malloc(p->n_gates);
	check_mem(i->permuta);

	i->vizinho = malloc(p->n_gates);
	check_mem(i->vizinho);

	i->prob = p;
	set_initialize_strategy(i, type);
	set_heuristics_strategy(i, p->heur);

	return i;
}

static void init_schema(individuo *ind)
{
	uchar pos, val, esquemas;
	problem *p = ind->prob;
	
	uchar *usado = malloc(p->n_gates * sizeof(*usado));
	check_mem(usado);

	memset(ind->permuta, NDEF, p->n_gates);
	memset(usado, FALSE, p->n_gates);
	
	// acredito que 'esquemas' aqui represente a quantidade de posi��es com informa��o
	esquemas = (p->n_gates * (1 - p->args.perc_schemas));

	for (uchar i = 0; i < esquemas; i++) {
		int chance = rand();
		val = (uchar) chance % p->n_gates;

		while (usado[val] != NDEF)
			val = val < p->n_gates - 1 ? val + 1 : 0; // fixme: trocar pela vers�o com resto (mod)

		usado[val] = TRUE;
		pos = (uchar) rand() % p->n_gates;

		while (ind->permuta[pos] != NDEF)
			pos = pos < p->n_gates - 1 ? pos + 1 : 0; // fixme: trocar pela vers�o com resto (mod)

		ind->permuta[pos] = val + 1;
	}

	// se percSchemas == 0, o indiv�duo � completo
	if (esquemas == p->n_gates) ind->tipo = FULL;

	//ind->sel = ind->mut = ind->col = 0;

	free(usado);
}

static void init_full(individuo *ind)
{
	problem *p = ind->prob;
	memset(ind->permuta, NDEF, p->n_gates);

	for (uchar i = 0; i < p->n_gates; i++) {
		uchar pos = (uchar) rand() % p->n_gates;

		while (ind->permuta[pos] != NDEF)
			pos = (++pos) % p->n_gates;

		ind->permuta[pos] = i + 1;
	}
}

static void set_initialize_strategy(individuo *ind, enum indi_t type)
{
	ind->tipo = type;

	switch (type) {
	case SCHEMA: ind->initialize = init_schema; break;
	case FULL:   ind->initialize = init_full;   break;
	default: die("%s", "Unknown individual type.");
	}
}

void init_individuo(individuo *ind) { ind->initialize(ind); }

void destroi_individuo(individuo *ind)
{
	free(ind->permuta); ind->permuta = NULL;
	free(ind->vizinho); ind->vizinho = NULL;
	free(ind); ind = NULL;
}

void copia_individuo(individuo *dest, individuo *src)
{
	memcpy(dest->permuta, src->permuta, dest->prob->n_gates);
	memcpy(dest->vizinho, src->vizinho, dest->prob->n_gates);

	void *permuta = dest->permuta;
	void *vizinho = dest->vizinho;

	memcpy(dest, src, sizeof(individuo));

	dest->permuta = permuta;
	dest->vizinho = vizinho;
}

float f_objetivo(individuo *ind)
{
	problem *p = ind->prob;

	uchar *esq = malloc(p->n_nets * sizeof(*esq));
	check_mem(esq);

	uchar *dir = malloc(p->n_nets * sizeof(*dir));
	check_mem(dir);

	for (int j = 0; j < p->n_nets; j++) {
		esq[j] = dir[j] = 0;

		for (int i = 0; i < p->n_gates; i++) {
			if (ind->permuta[i] != NDEF) {
				if (p->matrix[j][ind->permuta[i] - 1] == '1') {
					if (!esq[j])    esq[j] = (char) (i + 1);
					else            dir[j] = (char) (i + 1);
				}
			}
		}

		if (!dir[j])
			dir[j] = esq[j];
	}

	/* calculo das trilhas, custo e do numero de #�s */
	int i, j, max, custo, defs;
	float acum;
	for (i = 0, max = 0, custo = 0, defs = 0; i < p->n_gates; i++) {
		if (ind->permuta[i] != NDEF) {
			defs++;

			for (j = 0, acum = 0.0f; j < p->n_nets; j++)
				if (i >= esq[j] - 1 && i <= dir[j] - 1)
					acum += 1;

			max = (int) (acum > max ? acum : max);
			custo += (int) acum;
		}
	}

	ind->f = ind->g = p->n_gates * p->n_nets * max + custo;
	ind->trilhas = max;
	ind->tipo = (defs == p->n_gates ? FULL : SCHEMA);
	inc_cfo(p);

	free(esq);
	free(dir);

	return ind->g;
}

///////////////////////////////////
// heuristicas

float f_heuristica(individuo *ind)
{
	return ind->f_heuristics(ind);
}

float faggiolli(individuo *ind)
{
	int i, j, k;
	unsigned char aux;
	problem *p = ind->prob;

	struct {
		int abre, fech, cont, padr;
	} melhor = { 0 }, atual;

	individuo *J = cria_individuo(p, FULL);
	copia_individuo(J, ind);

	for (i = 0; i < p->n_gates - 2; i++)
	{
		if (J->permuta[i] != NDEF && J->permuta[i + 1] != NDEF) {
			melhor.abre = INT_MAX;
			melhor.fech = melhor.cont = -1;

			for (j = i + 1; j < p->n_gates; j++) {
				if (J->permuta[j] != NDEF) {
					atual.abre = atual.fech = atual.cont = 0;

					for (k = 0; k < p->n_nets; k++)
					{
						if (p->matrix[k][J->permuta[i] - 1] == '0' && p->matrix[k][J->permuta[j] - 1] == '1')
							atual.abre++;

						else if (p->matrix[k][J->permuta[i] - 1] == '1' && p->matrix[k][J->permuta[j] - 1] == '0')
							atual.fech++;

						else if (p->matrix[k][J->permuta[i] - 1] == '1' && p->matrix[k][J->permuta[j] - 1] == '1')
							atual.cont++;
					}

					if (atual.abre < melhor.abre ||
					    atual.abre == melhor.abre && atual.fech > melhor.fech ||
					    atual.abre == melhor.abre && atual.fech == melhor.fech && atual.cont > melhor.cont ||
					    atual.abre == melhor.abre && atual.fech == melhor.fech && atual.cont == melhor.cont && (rand() % 2)
					    ) {
						melhor = atual;
						melhor.padr = j;
					}
				}
			}

			aux = J->permuta[melhor.padr];
			J->permuta[melhor.padr] = J->permuta[i + 1];
			J->permuta[i + 1] = aux;
		}
	}


	float j_obj = f_objetivo(J); // f=g e trk unica para J
	float ind_obj = ind->g;

	if (j_obj < ind_obj) {
		ind->f_fag = ind->f = j_obj;
		ind->trilhar = J->trilhas;
		memcpy(ind->vizinho, J->permuta, p->n_gates);

	} else {
		ind->f_fag = j_obj;
		ind->trilhar = ind->trilhas;
		memcpy(ind->vizinho, ind->permuta, p->n_gates);
	}

	destroi_individuo(J);
	return ind->f_fag;
}

float two_opt(individuo *ind)
{
	problem *p = ind->prob;
	individuo *J = cria_individuo(p, FULL);

	for (int i = 0, sigmax = 0; i < p->n_gates - 1; i++) {
		for (int j = i + 1; j < p->n_gates; j++) {
			memcpy(J->permuta, ind->permuta, p->n_gates);
			opt_movement(J, i, j); // movimento 2OPT a partir das referencias
			double j_obj = f_objetivo(J);

			// calculo da distancia na permutacao (rank = 1 totalmente igual)
			if ((int) (ind->g - j_obj) > sigmax) {
				ind->f_opt = ind->f = j_obj;
				ind->trilhar = J->trilhas;
				memcpy(ind->vizinho, J->permuta, p->n_gates);

				destroi_individuo(J);
				return ind->f_opt;
			}
		}
	}

	ind->trilhar = ind->trilhas;
	memcpy(ind->vizinho, ind->permuta, p->n_gates);

	destroi_individuo(J);
	return ind->f;
}

float competitive(individuo *ind)
{
	int choice = rand() % 2;
	return choice ? two_opt(ind) : faggiolli(ind);
}

float parallel(individuo *ind)
{
	problem *p = ind->prob;
	int choice = p->world_rank % 2;
	return choice ? two_opt(ind) : faggiolli(ind);
}

float cooperative(individuo *ind)
{
	float f_fag = faggiolli(ind);
	float f_opt = two_opt(ind);

	return ind->f = MIN(f_fag, f_opt);
}

float cooperative_exp(individuo *ind)
{
	double f_fag = faggiolli(ind);
	double f_opt = two_opt(ind);

	return ind->f = MIN(f_fag, f_opt);
}

void opt_movement(individuo *ind, int p, int q)
{
	int i, j;
	char *a = malloc(ind->prob->n_gates);
	check_mem(a);

	memcpy(a, ind->permuta, ind->prob->n_gates);

	if (p + 1 == q) {
		j = 0;

		for (i = q; i < ind->prob->n_gates; i++, j++)
			ind->permuta[j] = a[i];

		for (i = 0; i <= p; i++, j++)
			ind->permuta[j] = a[i];
	} else {
		j = p + 1;

		for (i = q; i >= p + 1; i--, j++)
			ind->permuta[j] = a[i];
	}

	free(a);
}

///////////////////////////////////////
// deltas

float f_delta(individuo *ind)
{
	return ind->f_delta(ind);
}

float default_delta(individuo *ind)
{
	problem *p = ind->prob;
	float gmax = p->gmax;

	return ind->delta = (p->args.deviation  * (gmax - ind->g) - (ind->g - ind->f));
}

float coop_delta(individuo *ind)
{
	problem *p = ind->prob;
	float gmax = p->gmax;

	ind->delta = p->alfa * (gmax - ind->g) - p->beta * ((ind->g - ind->f_fag) + (ind->g - ind->f_opt));

	return ind->delta;
}

float coop_e_delta(individuo *ind)
{
	problem *p = ind->prob;
	float gmax = p->gmax;

	ind->delta = p->alfa * (gmax - ind->g) - p->beta * (exp((ind->g - ind->f_fag) + (ind->g - ind->f_opt)) - 1);

	return ind->delta;
}

static void set_heuristics_strategy(individuo *ind, enum heur_t heur)
{
	ind->f_delta = default_delta;

	switch (heur) {
	case FAG:   
		ind->f_heuristics = faggiolli;
		break;
	case OPT:   
		ind->f_heuristics = two_opt;
		break;
	case COMP:  
		ind->f_heuristics = competitive; 
		break;
	case COOP: 
		ind->f_heuristics = cooperative; 
		ind->f_delta = coop_delta;
		break;
	case COOP_E: 
		ind->f_heuristics = cooperative_exp; 
		ind->f_delta = coop_e_delta;
		break;
	case PARALLEL:
		ind->f_heuristics = parallel;
		break;

	default: die("%s", "Unknown heuristic.");
	}
}

void print_individuo(FILE *stream, individuo *ind, problem *p)
{
	#ifdef MTX
	for (int j = 0; j < p->n_gates; j++)
	{
		fprintf(stream, "\n\r");
		for (int i = 0; i < p->nNets; i++)
			if (ind->vizinho[j] != NDEF)
				fprintf(stream, " %c ", p->matrix[i][ind->vizinho[j] - 1]);
			else
				fprintf(stream, " ? ");
	}
	fprintf(stream, "\n\n\r");
	#endif

	#ifdef PERMUTA
	fprintf(stream, "\n\r");

	for (int j = 0; j < p->n_gates; j++) {
		if (ind->permuta[j] != NDEF)
			fprintf(stream, " %d ", ind->permuta[j]);
		else
			fprintf(stream, " # ");
	}
	fprintf(stream, "\n\r");

	for (int j = 0; j < p->n_gates; j++) {
		if (ind->vizinho[j] != NDEF)
			fprintf(stream, " %d ", ind->vizinho[j]);
		else
			fprintf(stream, " # ");
	}
	#endif

	fprintf(stream, "\n%c) G=%d (%d), F=%d (%d), ger/alf=%d/%.2f, %.6f, col/mut/sel=(%d,%d,%d), rank=%.4f\n",
		(ind->tipo == FULL ? 'C' : 'E'),
		(int) ind->g,
		(int) ind->trilhas,
		(int) ind->f,
		(int) ind->trilhar,
		ind->geracao,
		-1, //ind->alf,
		ind->delta,
		-1, //ind->col,
		-1, //ind->mut,
		-1, //ind->sel,
		-1 //ind->rank
		);
}

void show_permuta(FILE *stream, individuo *ind)
{
	problem *p = ind->prob;

	for (int i = 0; i < p->n_gates; i++) {
		printf("%d ", ind->permuta[i]);
	}
	printf("\n");
}