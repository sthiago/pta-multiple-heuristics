#ifndef _parse_opt_h_
#define _parse_opt_h_

#include "argp.h"
//#include "argp-namefrob.h"
//#include "argp-fmtstream.h"

// parameter parsing configuration
extern const char *argp_program_version;
extern const char *argp_program_bug_address;

/* program documentation. */
extern char doc[];

/* a description of the arguments we accept. */
extern char args_doc[];

/* the options we understand. */
extern struct argp_option options[];

/* used by 'main' to communicate with 'parse_opt'. */
struct arguments {
	char *args[1];		// problem file name
	int min_pop, largo, fator, amostra; // ?? o que s�o largo e fator?
	int max_loop, init_pop, n_steps, n_cross, verbose, xls, solution, max_cfo;
	float alfa1, alfa2, deviation, perc_schemas, perc_best, perc_mutation;
	char *type_name, *heur_name;
	enum prob_t type;
	enum heur_t heur;
	char *output_file;
	unsigned int seed;
	char *iteracao;
};

/* our argp parser. */
extern struct argp argp;

void set_default_params(struct arguments *args);
char *get_default_output(const char *input_file);

#endif
