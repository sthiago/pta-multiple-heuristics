#ifndef _problem_h_
#define _problem_h_

#include "parse_opt.h"

typedef struct _population population;
typedef struct _individuo individuo;

//#define NOLOG
//#define NOREPRO
//#define ANSI
//#define PERMUTA
//#define NOMTX
//#define DUMP
//#define EXTRA
//#define DEBUG

/* Parametros de desempenho fixos */

//#define INFINITO 65535000

// S�mbolos e constantes

enum { FALSE, TRUE };
enum prob_t { MOSP, VLSI }; // tipos de problema
enum heur_t { FAG, OPT, COOP, COOP_E, COMP, PARALLEL }; // tipos de heur�stica

/* Herdados */
//#define RAND 1
//#define ABRE 1
//#define FECHA 0
//#define TAXAPD .8
//#define SOLUCAO 14
//#define AMOSTRA  5
//#define AMOSTRACFO 10

typedef unsigned char uchar;
//typedef unsigned int  uint;
typedef unsigned long ulong;

typedef struct _problem {
	// mpi info
	int world_rank;
	int world_size;
	int new_best;

	// info
	char **matrix;
	uchar n_gates, n_nets;
	float perc_schemas; // percentual de schemas
	float alfa, beta; // para c�lculo na heur�stica cooperativa (pesos)
	enum prob_t type;
	enum heur_t heur;
	int n_cfo; // n�mero de chamaras � fun��o objetivo
	FILE *log_file;

	// arguments
	struct arguments args;

	// genetics
	population *pop;
	individuo *best;
	int generation;
	float gmax;
} problem;

problem *init_problem(int, char **);
void inc_cfo(problem *p);

#endif
