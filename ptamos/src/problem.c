#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "dbg.h"
#include "problem.h"
#include "population.h"
#include "individuo.h"

char **alloc_matrix(int m, int n);

void parse_file(FILE *fp, problem *p);
void parse_mosp(FILE *fp, problem *p);
void parse_vlsi(FILE *fp, problem *p);

void show_params(FILE *stream, const struct arguments *a);
void show_prob(FILE *stream, const problem *p);

problem *init_problem(int argc, char **argv)
{
	// create problem
	problem *p = malloc(sizeof *p);
	check_mem(p);
	struct arguments args;

	// mpi init
	MPI_Comm_rank(MPI_COMM_WORLD, &p->world_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p->world_size);
	p->new_best = FALSE;

	// argument parsing and setting
	set_default_params(&args);
	argp_parse(&argp, argc, argv, 0, 0, &args);

	// set default output name if none provided
	if (strcmp(args.output_file, "-") == 0)
		args.output_file = get_default_output(args.args[0]);

	// open problem file
	FILE *problem_file = fopen(args.args[0], "r");
	check(problem_file, "Failed to open %s", args.args[0]);

	// open output file
	FILE *log_file = fopen(args.output_file, "a");
	p->log_file = log_file;
	check(log_file, "Failed to open %s", args.output_file);

	// problem setup
	if (args.verbose) log_info("%s", "setting up problem\n");
	p->n_cfo = 0;
	if (fscanf(problem_file, "%hhu %hhu ", &p->n_gates, &p->n_nets) != 2)
		die("%s", "error reading input file");

	p->args = args;
	p->type = args.type;
	p->heur = args.heur;

	// TODO: colocar como par�metro alfa e beta
	p->alfa = 0.6f;
	p->beta = 0.4f; 

	// parameter and problem logging
	if (args.verbose) {
		log_info("%s", "showing parameters: ");
		show_params(stdout, &args);

		log_info("%s", "showing problem info: ");
		show_prob(stdout, p);
	}

	if (!args.xls) {
		show_params(log_file, &args);
		show_prob(log_file, p);
	}

	// allocate initial matrix
	if (args.verbose) log_info("%s", "allocating initial memory");
	p->matrix = alloc_matrix(p->n_nets, p->n_gates);
	check(p->matrix, "%s", "matrix allocation error");

	// parse problem file
	if (args.verbose) log_info("parsing %s file", args.args[0]);
	parse_file(problem_file, p);
	fclose(problem_file);

	// genetics setup
	if (args.verbose) log_info("%s", "setting up genetics");
	srand(args.seed * (p->world_rank + 1) + p->world_rank);

	// initialize population
	p->pop = malloc(sizeof *p->pop);
	check_mem(p->pop);

	p->pop->inicio = p->pop->fim = NULL;
	p->pop->tamanho = 0;
	p->gmax = 0.f;

	// initialize best individual
	p->best = cria_individuo(p, FULL);
	init_individuo(p->best);

	f_objetivo(p->best);
	f_heuristica(p->best);
	f_delta(p->best);

	// superior limit
	p->gmax = 2 * p->best->g;

	// initial guess
	p->best->geracao = 0;
		
	return p;

error:
	exit(-1);
}

char **alloc_matrix(int m, int n)
{
	char **matrix = malloc(m * sizeof(*matrix));
	check_mem(matrix);

	for (int i = 0; i < m; i++) {
		matrix[i] = malloc(n * sizeof(**matrix));
		check_mem(matrix[i]);
	}

	return matrix;
}

void parse_file(FILE *fp, problem *p)
{
	switch (p->type) {
	case MOSP:
		parse_mosp(fp, p);
		break;
	case VLSI:
		parse_vlsi(fp, p);
		break;
	default:
		log_err("%s", "Unknown problem type");
		exit(-1);
	}
}

void parse_mosp(FILE *fp, problem *p)
{
	check(fp, "%s", "MOSP file error.");

	int m = p->n_gates,
		n = p->n_nets;

	int i = 0, j = 0;
	for (i = 0; i < m && !feof(fp); i++) {
		for (j = 0; j < n && !feof(fp); j++) {

			// to do: adicionar mensagem de erro 
			char ch;
			if (fscanf(fp, "%c\n", &ch) != 1) {
				exit(-1);
			}

			p->matrix[j][i] = ch == '0' ? '0' : '1';
		}
	}

	check(j == n && i == m, "%s", "Matriz invalida.");

	return;

error:
	exit(-1);
}

void parse_vlsi(FILE *fp, problem *p)
{
	check(fp, "%s", "VLSI file error.");

	int n = p->n_gates,
		m = p->n_nets;

	int i, j = 0;
	for (i = 0; i < m && !feof(fp); i++) {
		for (j = 0; j < n && !feof(fp);) {
			char ch;
			fread(&ch, sizeof(char), 1, fp);
			if (ch == '1' || ch == '0') {
				p->matrix[i][j] = ch;
				j++;
			}
		}
	}

	check(j == n && i == m, "%s", "Matriz invalida.");

	return;

error:
	exit(-1);
}

void show_params(FILE *stream, const struct arguments *a)
{
	fprintf(stream, "Verbose mode:              %s\n", a->verbose ? "Yes" : "No");
	fprintf(stream, "CSV-like output mode:      %s\n", a->xls ? "Yes" : "No");
	fprintf(stream, "Max. num. of generations:  %d\n", a->max_loop);
	fprintf(stream, "Initial population size:   %d\n", a->init_pop);
	fprintf(stream, "Num. of crossovers:        %d\n", a->n_cross);
	fprintf(stream, "Num. of mutation steps:    %d\n", a->n_steps);
	fprintf(stream, "Perc. of mutation:         %.4f\n", a->perc_mutation);
	fprintf(stream, "Perc. of best individuals: %.4f\n", a->perc_best);
	fprintf(stream, "Deviation:                 %.4f\n", a->deviation);
	fprintf(stream, "Input file name:           %s\n", a->args[0]);
	fprintf(stream, "Output file name:          %s\n", a->output_file);
	fprintf(stream, "\n");
}

void show_prob(FILE *stream, const problem *p)
{
	fprintf(stream, "Min. population: %d\n", p->args.min_pop);
	fprintf(stream, "Type:            %s\n", p->args.type_name);
	fprintf(stream, "Heuristic:       %s\n", p->args.heur_name);
	fprintf(stream, "Largo (?):       %d\n", p->args.largo);
	fprintf(stream, "Fator (?):       %d\n", p->args.fator);
	fprintf(stream, "Num. gates:      %d\n", p->n_gates);
	fprintf(stream, "Num. nets:       %d\n", p->n_nets);
	fprintf(stream, "\n");
}

void inc_cfo(problem *p)
{
	p->n_cfo++;

	//if (p->n_cfo % 100 == 0) {
	//	char filename[1024] = { 0 };
	//	strcat(filename, "relatorio_cfo_");
	//	strcat(filename, p->args.heur_name);
	//	strcat(filename, "_");
	//	strcat(filename, p->args.iteracao);
	//	strcat(filename, "_");
	//	strcat(filename, p->args.args[0]);

	//	FILE *relatorio = fopen(filename, "a");
	//	fprintf(relatorio, "%d\t%d\t%d\n", p->n_cfo, (int) p->best->g, (int) p->best->trilhar);
	//	fclose(relatorio);

	//	//FILE *fp = fopen("evolucao_ind.txt", "a");
	//	//fprintf(fp, "%f\n", evolution->best->g);
	//	//fclose(fp);
	//}
}