#ifndef _population_h_
#define _population_h_

typedef struct _problem problem;
typedef struct _individuo individuo;

typedef struct _elemento {
	individuo         *indi;
	struct _elemento  *ante;
	struct _elemento  *prox;
} elemento;

typedef struct _population {
	elemento *inicio, *fim;
	unsigned tamanho;
} population;

void init_population(problem *p);
void insere_individuo(population *p, individuo *i);
individuo *seleciona_base(population *pop);
individuo *seleciona_guia(population *pop);
int mutacao(individuo *ind);
individuo *recombina_BO(individuo *base, individuo *guia);
individuo *poda_populacao(population *p, double alfa);

#endif