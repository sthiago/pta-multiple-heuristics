#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* If this is being built for a unit test. */
#ifdef UNIT_TESTING

/* Redirect assert to mock_assert() so assertions can be caught by cmocka. */
#ifdef assert
#undef assert
#endif /* assert */
#define assert(expression) \
    mock_assert((int)(expression), #expression, __FILE__, __LINE__)
void mock_assert(const int result, const char* expression, const char *file,
		 const int line);

/* Redirect calloc and free to test_calloc() and test_free() so cmocka can
* check for memory leaks. */
#ifdef calloc
#undef calloc
#endif /* calloc */
#define calloc(num, size) test_calloc(num, size, __FILE__, __LINE__)

#ifdef malloc
#undef malloc
#endif /* malloc */
#define malloc(num, size) test_malloc(num, size, __FILE__, __LINE__)

#ifdef free
#undef free
#endif /* free */
#define free(ptr) test_free(ptr, __FILE__, __LINE__)

void* _test_calloc(const size_t number_of_elements, const size_t size,
		   const char* file, const int line);

void _test_free(void* const ptr, const char* file, const int line);

#endif /* UNIT_TESTING */