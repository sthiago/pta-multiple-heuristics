#define UNIT_TESTING

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include "cmocka.h"

#include "individuo.h"
#include "individuo.c"
#include "ptamosp.h"

static int setup(void **state) {
	problem *p = calloc(sizeof *p, 1);
	if (p == NULL) return -1;

	p->nGates = 10;
	p->percSchemas = 0.f;

	*state = p;

	return 0;
}

static void test_novo_individuo(void **state) {
	individuo *ind = novo_individuo(*state, SCHEMA);
	inicializa_individuo(ind);

	assert_non_null(ind);
	assert_non_null(ind->permuta);
	assert_non_null(ind->vizinho);
	assert_non_null(ind->initialize);

	assert_ptr_equal(ind->prob, *state);
	destroi_individuo(ind);

	ind = novo_individuo(*state, FULL);
	inicializa_individuo(ind);

	assert_non_null(ind);
	assert_non_null(ind->permuta);
	assert_non_null(ind->vizinho);
	assert_non_null(ind->initialize);

	assert_ptr_equal(ind->prob, *state);
	destroi_individuo(ind);
}

static void test_copia_individuo(void **state) {
	individuo *src = novo_individuo(*state, FULL);
	individuo *dest = novo_individuo(*state, SCHEMA);

	src->f = 1.2f;
	src->g = 3.4f;

	src->permuta[1] = 1;
	src->permuta[3] = 30;

	copia_individuo(dest, src);

	assert_true(dest->f == 1.2f);
	assert_true(dest->g == 3.4f);
	assert_true(dest->permuta[1] == 1);
	assert_true(dest->permuta[3] == 30);

	assert_ptr_equal(src->prob, dest->prob);
	assert_ptr_not_equal(src, dest);
	assert_ptr_not_equal(src->permuta, dest->permuta);
	assert_ptr_not_equal(src->vizinho, dest->vizinho);

	dest->permuta[1] = 2;

	assert_true(dest->permuta[1] != src->permuta[1]);
}

static void test_destroi_individuo(void **state)
{
	individuo *ind1 = novo_individuo(*state, FULL);
	individuo *ind2 = novo_individuo(*state, SCHEMA);

	inicializa_individuo(ind1);
	inicializa_individuo(ind2);

	//destroi_individuo(ind);

	//assert_null(ind);
}


int main(void) {
	const struct CMUnitTest tests[] = {
		cmocka_unit_test_setup(test_novo_individuo, setup),
		cmocka_unit_test_setup(test_copia_individuo, setup),
		cmocka_unit_test_setup(test_destroi_individuo, setup),
	};

	return cmocka_run_group_tests_name("individuo", tests, NULL, NULL);
}
